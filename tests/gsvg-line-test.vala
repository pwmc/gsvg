/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * gsvg-test.vala

 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;

class GSvgTest.Suite : GLib.Object
{
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/gsvg/line/style",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        var l = svg.create_line ("0","0", "10", "10", null);
        svg.append_child (l);
        message (l.write_string ());
        assert ("""<line x1="0" y1="0" x2="10" y2="10"/>""" in l.write_string ());
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/line/read",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        string str = """<?xml version="1.0"?>
  <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1e+03 3e+02" width="10cm" height="3cm"><desc>Example tspan01 - using tspan to change visual attributes</desc><g font-family="Verdana" font-size="45"><text fill="blue" x="200" y="150">You are <tspan font-weight="bold" fill="red">not</tspan> a banana</text></g><circle id="circle" fill="none" stroke="blue" stroke-width="2" cx="1" cy="1" r="250"/><line stroke="green" stroke-width="2" id="line" x1="1" y1="1" x2="5" y2="5"/></svg>""";
        svg.read_from_string (str);
        var sh = svg.get_element_by_id ("line") as DomLineElement;
        assert (sh != null);
        assert (sh is DomLineElement);
        assert (sh.x1 != null);
        assert (sh.x1.value == "1");
        assert (sh.y1 != null);
        assert (sh.y1.value == "1");
        assert (sh.x2 != null);
        assert (sh.x2.value == "5");
        assert (sh.y2 != null);
        assert (sh.y2.value == "5");
        assert (sh.id == "line");
        assert (svg.lines != null);
        message (svg.write_string ());
        message (svg.lines_map.length.to_string ());
        assert (sh.node_name == "line");
        assert (svg.get_elements_by_tag_name ("line").length == 1);
        assert (svg.lines is DomLineElementMap);
        assert (svg.lines is LineElementMap);
        assert (svg.lines is DomLineElementMap);
        assert (svg.lines_map.items_type.is_a (typeof (DomLineElement)));
        assert (svg.lines.length == 1);
        assert (svg.lines.length == 1);
        assert (svg.lines.get ("line") is DomLineElement);
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    return Test.run ();
  }
}
