/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * gsvg-path-test.vala

 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using GXml;

class GSvgTest.Suite : GLib.Object
{
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/gsvg/path/write/d",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        var p = svg.create_path ("M 100 100 L 200 200 L 300 100 z", null);
        assert (p is DomPathElement);
        message (((GXml.Element) p).write_string ());
        svg.append_child (p);
        message (svg.write_string ());
        assert ("""<svg xmlns="http://www.w3.org/2000/svg"><path d="M 100 100 L 200 200 L 300 100 z"/>""" in svg.write_string ());
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/path/write/length",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        var p = svg.create_path ("M 100 100 L 200 200 L 300 100 z", "300");
        assert (p is DomPathElement);
        message (((GXml.Element) p).write_string ());
        svg.append_child (p);
        message (svg.write_string ());
        assert ("""d="M 100 100 L 200 200 L 300 100 z"/>""" in svg.write_string ());
        assert ("pathLength=\"300\"" in svg.write_string ());
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/path/read/d",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        svg.read_from_string ("""<svg xmlns="http://www.w3.org/2000/svg"><path id="path" d="M 100 100 L 200 200 L 300 100 z"/></svg>""");
        assert (svg.child_nodes.length == 1);
        var p = svg.child_nodes.item (0) as DomPathElement;
        assert (p != null);
        assert (p is DomPathElement);
        assert (p is PathElement);
        assert (((PathElement) p).d == "M 100 100 L 200 200 L 300 100 z");
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/path/read/length",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        svg.read_from_string ("""<svg xmlns="http://www.w3.org/2000/svg"><path id="path" d="M 100 100 L 200 200 L 300 100 z" pathLength="300" /></svg>""");
        assert (svg.child_nodes.length == 1);
        var p = svg.child_nodes.item (0) as DomPathElement;
        assert (p != null);
        assert (p is DomPathElement);
        assert (p is PathElement);
        assert (p.path_length.value == "300");
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/path/parse/parameters",
    ()=>{
      string str = "M20.20 30.28 m 120 134";
      double x = 0.0;
      double y = 0.0;
      string unparsed = null;
      string command = DomPathSegList.parse_command (str, out unparsed);
      assert (command == "M");
      assert (unparsed == "20.20 30.28 m 120 134");
      unparsed = DomPoint.parse_point_values (str, out x, out y);
      assert (x == 20.2);
      assert (y == 30.28);
      message (unparsed);
      assert (unparsed == " m 120 134");
      x = 0.0;
      y = 0.0;
      command = DomPathSegList.parse_command (unparsed, out unparsed);
      assert (command == "m");
      DomPoint.parse_point_values (unparsed, out x, out y);
      assert (x == 120.0);
      assert (y == 134.0);
      str = "M20,50 m -20,50";
      unparsed = str.replace (",", " ");
      x = 0.0;
      y = 0.0;
      command = DomPathSegList.parse_command (unparsed, out unparsed);
      assert (command == "M");
      assert (unparsed == "20 50 m -20 50");
      message (unparsed);
      unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
      assert (x == 20.0);
      assert (y == 50.0);
      message (unparsed);
      command = DomPathSegList.parse_command (unparsed, out unparsed);
      message (unparsed);
      assert (command == "m");
      unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
      assert (x == -20.0);
      assert (y == 50.0);
    });
    Test.add_func ("/gsvg/path/parse/moveto",
    ()=>{
      try {
        string str = "M20.20 30.28 m 120 134";
        var l = new PathSegList ();
        l.parse (str);
        assert (l.number_of_items == 2);
        var s1 = l.get_item (0) as DomPathSegMovetoAbs;
        assert (s1 != null);
        assert (s1 is DomPathSegMovetoAbs);
        assert (s1.x == 20.2);
        assert (s1.y == 30.28);
        var s2 = l.get_item (1) as DomPathSegMovetoRel;
        assert (s2 != null);
        assert (s2 is DomPathSegMovetoRel);
        assert (s2.x == 120.0);
        assert (s2.y == 134.0);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/write/moveto",
    ()=>{
      try {
        string str = "M20.2,30.28 m120,134";
        var l = new PathSegList ();
        var s1 = new PathSegMovetoAbs ();
        s1.x = 20.2;
        s1.y = 30.28;
        l.append_item (s1);
        var s2 = new PathSegMovetoRel ();
        s2.x = 120.0;
        s2.y = 134.0;
        l.append_item (s2);
        assert (l.number_of_items == 2);
        message (l.to_string ());
        assert (l.to_string () == str);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/parse/closepath",
    ()=>{
      try {
        string str = "M20.20 30.28 m 120 134 z";
        var l = new PathSegList ();
        l.parse (str);
        assert (l.number_of_items == 3);
        var s1 = l.get_item (0) as DomPathSegMovetoAbs;
        assert (s1 != null);
        assert (s1 is DomPathSegMovetoAbs);
        assert (s1.x == 20.2);
        assert (s1.y == 30.28);
        var s2 = l.get_item (1) as DomPathSegMovetoRel;
        assert (s2 != null);
        assert (s2 is DomPathSegMovetoRel);
        assert (s2.x == 120.0);
        assert (s2.y == 134.0);
        var s3 = l.get_item (2) as DomPathSegClosePath;
        assert (s3 != null);
        assert (s3 is DomPathSegClosePath);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/write/closepath",
    ()=>{
      try {
        string str = "M20.2,30.28 z";
        var l = new PathSegList ();
        var s1 = new PathSegMovetoAbs ();
        s1.x = 20.2;
        s1.y = 30.28;
        l.append_item (s1);
        var s2 = new PathSegClosePath ();
        l.append_item (s2);
        assert (l.number_of_items == 2);
        message (l.to_string ());
        assert (l.to_string () == str);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/parse/lineto",
    ()=>{
      try {
        string str = "L20.20 30.28 l 120 134";
        var l = new PathSegList ();
        l.parse (str);
        assert (l.number_of_items == 2);
        var s1 = l.get_item (0) as DomPathSegLinetoAbs;
        assert (s1 != null);
        assert (s1 is DomPathSegLinetoAbs);
        assert (s1.x == 20.2);
        assert (s1.y == 30.28);
        var s2 = l.get_item (1) as DomPathSegLinetoRel;
        assert (s2 != null);
        assert (s2 is DomPathSegLinetoRel);
        assert (s2.x == 120.0);
        assert (s2.y == 134.0);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/write/lineto",
    ()=>{
      try {
        string str = "L20.2,30.28 l-120.1,134";
        var l = new PathSegList ();
        var s1 = new PathSegLinetoAbs ();
        s1.x = 20.2;
        s1.y = 30.28;
        l.append_item (s1);
        var s2 = new PathSegLinetoRel ();
        s2.x = -120.1;
        s2.y = 134.0;
        l.append_item (s2);
        assert (l.number_of_items == 2);
        message (l.to_string ());
        assert (l.to_string () == str);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/parse/lineto/horizontal",
    ()=>{
      try {
        string str = "L20.20 30.28 H 120 h 140";
        var l = new PathSegList ();
        l.parse (str);
        assert (l.number_of_items == 3);
        var s1 = l.get_item (0) as DomPathSegLinetoAbs;
        assert (s1 != null);
        assert (s1 is DomPathSegLinetoAbs);
        assert (s1.x == 20.2);
        assert (s1.y == 30.28);
        var s2 = l.get_item (1) as DomPathSegLinetoHorizontalAbs;
        assert (s2 != null);
        assert (s2 is DomPathSegLinetoHorizontalAbs);
        assert (s2.x == 120.0);
        var s3 = l.get_item (2) as DomPathSegLinetoHorizontalRel;
        assert (s3 != null);
        assert (s3 is DomPathSegLinetoHorizontalRel);
        assert (s3.x == 140.0);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/write/lineto/horizontal",
    ()=>{
      try {
        string str = "L20.2,30.28 H-120.1 h4.2";
        var l = new PathSegList ();
        var s1 = new PathSegLinetoAbs ();
        s1.x = 20.2;
        s1.y = 30.28;
        l.append_item (s1);
        var s2 = new PathSegLinetoHorizontalAbs ();
        s2.x = -120.1;
        l.append_item (s2);
        var s3 = new PathSegLinetoHorizontalRel ();
        s3.x = 4.2;
        l.append_item (s3);
        assert (l.number_of_items == 3);
        message (l.to_string ());
        assert (l.to_string () == str);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/parse/lineto/vertical",
    ()=>{
      try {
        string str = "L20.20 30.28 V 120 v 140";
        var l = new PathSegList ();
        l.parse (str);
        assert (l.number_of_items == 3);
        var s1 = l.get_item (0) as DomPathSegLinetoAbs;
        assert (s1 != null);
        assert (s1 is DomPathSegLinetoAbs);
        assert (s1.x == 20.2);
        assert (s1.y == 30.28);
        var s2 = l.get_item (1) as DomPathSegLinetoVerticalAbs;
        assert (s2 != null);
        assert (s2 is DomPathSegLinetoVerticalAbs);
        assert (s2.y == 120.0);
        var s3 = l.get_item (2) as DomPathSegLinetoVerticalRel;
        assert (s3 != null);
        assert (s3 is DomPathSegLinetoVerticalRel);
        assert (s3.y == 140.0);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/write/lineto/vertical",
    ()=>{
      try {
        string str = "L20.2,30.28 V-120.1 v4.2";
        var l = new PathSegList ();
        var s1 = new PathSegLinetoAbs ();
        s1.x = 20.2;
        s1.y = 30.28;
        l.append_item (s1);
        var s2 = new PathSegLinetoVerticalAbs ();
        s2.y = -120.1;
        l.append_item (s2);
        var s3 = new PathSegLinetoVerticalRel ();
        s3.y = 4.2;
        l.append_item (s3);
        assert (l.number_of_items == 3);
        message (l.to_string ());
        assert (l.to_string () == str);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/parse/curveto",
    ()=>{
      try {
        string str = "C20.20 30.28 100 120 16.5 14.5 c 120 140 200 400 3.4 2.5";
        var l = new PathSegList ();
        l.parse (str);
        assert (l.number_of_items == 2);
        var s1 = l.get_item (0) as PathSegCurvetoCubicAbs;
        assert (s1 != null);
        assert (s1 is PathSegCurvetoCubicAbs);
        assert (s1.x1 == 20.20);
        assert (s1.y1 == 30.28);
        assert (s1.x2 == 100.0);
        assert (s1.y2 == 120.0);
        assert (s1.x == 16.5);
        assert (s1.y == 14.5);
        var s2 = l.get_item (1) as PathSegCurvetoCubicRel;
        assert (s2 != null);
        assert (s2 is PathSegCurvetoCubicRel);
        assert (s2.x1 == 120.0);
        assert (s2.y1 == 140.0);
        assert (s2.x2 == 200.0);
        assert (s2.y2 == 400.0);
        assert (s2.x == 3.4);
        assert (s2.y == 2.5);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/write/curveto",
    ()=>{
      try {
        string str = "C20.2,30.28 5.1,10.6 10.1,15.6 c-120.1,100 50,1 20,50";
        var l = new PathSegList ();
        var s1 = new PathSegCurvetoCubicAbs ();
        s1.x1 = 20.2;
        s1.y1 = 30.28;
        s1.x2 = 5.1;
        s1.y2 = 10.6;
        s1.x = 10.1;
        s1.y = 15.6;
        l.append_item (s1);
        var s2 = new PathSegCurvetoCubicRel ();
        s2.x1 = -120.1;
        s2.y1 = 100.0;
        s2.x2 = 50.0;
        s2.y2 = 1.0;
        s2.x = 20.0;
        s2.y = 50.0;
        l.append_item (s2);
        assert (l.number_of_items == 2);
        message (l.to_string ());
        assert (l.to_string () == str);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/parse/curveto/smooth",
    ()=>{
      try {
        string str = "S20.20 30.28 100 120 s 120 140 200 400";
        var l = new PathSegList ();
        l.parse (str);
        assert (l.number_of_items == 2);
        var s1 = l.get_item (0) as PathSegCurvetoCubicSmoothAbs;
        assert (s1 != null);
        assert (s1 is PathSegCurvetoCubicSmoothAbs);
        assert (s1.x2 == 20.20);
        assert (s1.y2 == 30.28);
        assert (s1.x == 100.0);
        assert (s1.y == 120.0);
        var s2 = l.get_item (1) as PathSegCurvetoCubicSmoothRel;
        assert (s2 != null);
        assert (s2 is PathSegCurvetoCubicSmoothRel);
        assert (s2.x2 == 120.0);
        assert (s2.y2 == 140.0);
        assert (s2.x == 200.0);
        assert (s2.y == 400.0);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/write/curveto/smooth",
    ()=>{
      try {
        string str = "S20.2,30.28 10.1,15.6 s-120.1,100 20,50";
        var l = new PathSegList ();
        var s1 = new PathSegCurvetoCubicSmoothAbs ();
        s1.x2 = 20.2;
        s1.y2 = 30.28;
        s1.x = 10.1;
        s1.y = 15.6;
        l.append_item (s1);
        var s2 = new PathSegCurvetoCubicSmoothRel ();
        s2.x2 = -120.1;
        s2.y2 = 100.0;
        s2.x = 20.0;
        s2.y = 50.0;
        l.append_item (s2);
        assert (l.number_of_items == 2);
        message (l.to_string ());
        assert (l.to_string () == str);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/parse/curveto/quadratic",
    ()=>{
      try {
        string str = "Q20.20 30.28 100 120 q 120 140 200 400";
        var l = new PathSegList ();
        l.parse (str);
        assert (l.number_of_items == 2);
        var s1 = l.get_item (0) as PathSegCurvetoQuadraticAbs;
        assert (s1 != null);
        assert (s1 is PathSegCurvetoQuadraticAbs);
        assert (s1.x1 == 20.20);
        assert (s1.y1 == 30.28);
        assert (s1.x == 100.0);
        assert (s1.y == 120.0);
        var s2 = l.get_item (1) as PathSegCurvetoQuadraticRel;
        assert (s2 != null);
        assert (s2 is PathSegCurvetoQuadraticRel);
        assert (s2.x1 == 120.0);
        assert (s2.y1 == 140.0);
        assert (s2.x == 200.0);
        assert (s2.y == 400.0);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/write/curveto/quadratic",
    ()=>{
      try {
        string str = "Q20.2,30.28 10.1,15.6 q-120.1,100 20,50";
        var l = new PathSegList ();
        var s1 = new PathSegCurvetoQuadraticAbs ();
        s1.x1 = 20.2;
        s1.y1 = 30.28;
        s1.x = 10.1;
        s1.y = 15.6;
        l.append_item (s1);
        var s2 = new PathSegCurvetoQuadraticRel ();
        s2.x1 = -120.1;
        s2.y1 = 100.0;
        s2.x = 20.0;
        s2.y = 50.0;
        l.append_item (s2);
        assert (l.number_of_items == 2);
        message (l.to_string ());
        assert (l.to_string () == str);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/parse/curveto/quadratic/smooth",
    ()=>{
      try {
        string str = "T100 120 t 200 400";
        var l = new PathSegList ();
        l.parse (str);
        assert (l.number_of_items == 2);
        var s1 = l.get_item (0) as PathSegCurvetoQuadraticSmoothAbs;
        assert (s1 != null);
        assert (s1 is PathSegCurvetoQuadraticSmoothAbs);
        assert (s1.x == 100.0);
        assert (s1.y == 120.0);
        var s2 = l.get_item (1) as PathSegCurvetoQuadraticSmoothRel;
        assert (s2 != null);
        assert (s2 is PathSegCurvetoQuadraticSmoothRel);
        assert (s2.x == 200.0);
        assert (s2.y == 400.0);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/write/curveto/quadratic/smooth",
    ()=>{
      try {
        string str = "T10.1,15.6 t-20,50";
        var l = new PathSegList ();
        var s1 = new PathSegCurvetoQuadraticSmoothAbs ();
        s1.x = 10.1;
        s1.y = 15.6;
        l.append_item (s1);
        var s2 = new PathSegCurvetoQuadraticSmoothRel ();
        s2.x = -20.0;
        s2.y = 50.0;
        l.append_item (s2);
        assert (l.number_of_items == 2);
        message (l.to_string ());
        assert (l.to_string () == str);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/parse/arc",
    ()=>{
      try {
        string str = "A25,25 30 0,1 50,25 a 5.5,-2.5 3 1,0 6.5,-2.5";
        var l = new PathSegList ();
        l.parse (str);
        assert (l.number_of_items == 2);
        var s1 = l.get_item (0) as PathSegArcAbs;
        assert (s1 != null);
        assert (s1 is PathSegArcAbs);
        assert (s1.r1 == 25.0);
        assert (s1.r2 == 25.0);
        assert (s1.angle == 30.0);
        assert (!s1.large_arc_flag);
        assert (s1.sweep_flag);
        assert (s1.x == 50.0);
        assert (s1.y == 25.0);
        var s2 = l.get_item (1) as PathSegArcRel;
        assert (s2 != null);
        assert (s2 is PathSegArcRel);
        assert (s2.r1 == 5.5);
        assert (s2.r2 == -2.50);
        assert (s2.angle == 3.0);
        assert (s2.large_arc_flag);
        assert (!s2.sweep_flag);
        assert (s2.x == 6.5);
        assert (s2.y == -2.5);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/path/write/arc",
    ()=>{
      try {
        string str = "A10.1,15.6 -30 1,0 20,10 a-20,50 40 0,1 50,20";
        var l = new PathSegList ();
        var s1 = new PathSegArcAbs ();
        s1.r1 = 10.1;
        s1.r2 = 15.6;
        s1.angle = -30.0;
        s1.large_arc_flag = true;
        s1.sweep_flag = false;
        s1.x = 20.0;
        s1.y = 10.0;
        l.append_item (s1);
        var s2 = new PathSegArcRel ();
        s2.r1 = -20.0;
        s2.r2 = 50.0;
        s2.angle = 40.0;
        s2.large_arc_flag = false;
        s2.sweep_flag = true;
        s2.x = 50.0;
        s2.y = 20.0;
        l.append_item (s2);
        assert (l.number_of_items == 2);
        message (l.to_string ());
        assert (l.to_string () == str);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });

    Test.add_func ("/gsvg/path/assign",
    ()=>{
      try {
        var pe = new PathElement () as DomPathElement;
        pe.d = "T10.1,15.6 t-20,50";
        assert (pe.d != null);
        var lp = pe.path_seg_list;
        assert (lp.number_of_items == 2);
        var ts1 = lp.get_item (0);
        assert (ts1 != null);
        assert (ts1 is DomPathSeg);
        var ts2 = lp.get_item (1);
        assert (ts2 != null);
        assert (ts2 is DomPathSeg);
        var l = new PathSegList ();
        string str = "A10.1,15.6 -30 1,0 20,10 a-20,50 40 0,1 50,20";
        var s1 = new PathSegArcAbs ();
        s1.r1 = 10.1;
        s1.r2 = 15.6;
        s1.angle = -30.0;
        s1.large_arc_flag = true;
        s1.sweep_flag = false;
        s1.x = 20.0;
        s1.y = 10.0;
        l.append_item (s1);
        var s2 = new PathSegArcRel ();
        s2.r1 = -20.0;
        s2.r2 = 50.0;
        s2.angle = 40.0;
        s2.large_arc_flag = false;
        s2.sweep_flag = true;
        s2.x = 50.0;
        s2.y = 20.0;
        l.append_item (s2);
        assert (l.number_of_items == 2);
        message (l.to_string ());
        assert (l.to_string () == str);
        pe.d = l.to_string ();
        assert (pe.d == str);
        assert (pe.path_seg_list.to_string () == str);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    return Test.run ();
  }
}
