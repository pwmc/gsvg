/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * gsvg-css-parsing-test.vala

 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using GXml;

class GSvgTest.Suite : GLib.Object
{
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/gsvg/element/pick/circle",
    ()=>{
      try {
        var d = new GSvg.Document ();
        assert (d.root_element.pixel_unit_to_millimeter_x == 96/25.4);
        assert (d.root_element.pixel_unit_to_millimeter_y == 96/25.4);
        d.root_element.pixel_unit_to_millimeter_x = 0.1;
        d.root_element.pixel_unit_to_millimeter_y = 0.1;
        assert (d.root_element.pixel_unit_to_millimeter_x == 0.1);
        assert (d.root_element.pixel_unit_to_millimeter_y == 0.1);
        var r = d.root_element;
        var p = new Point ();
        p.x = 100;
        p.y = 0;
        // Filled
        var c = r.create_circle ("10mm", "10mm", "10mm", "stroke: white; stroke-width: 1mm");
        r.append_child (c);
        message (c.write_string ());
        assert (c.is_filled ());
        assert (c.style != null);
        var cssv = c.style.get_property_value ("stroke-width");
        assert (cssv != null);
        var l = c.stroke_width_to_lenght ();
        message ("L = %g", l.value);
        assert (l.value == 1);
        assert (l.unit_type == DomLength.Type.MM);
        message ("X pixels in lenght = %g", r.convert_length_x_pixels (l));
        message ("Y pixels in lenght = %g", r.convert_length_y_pixels (l));
        assert (r.convert_length_x_pixels (l) == 1/0.1);
        assert (c.pick (p, 10));
        p.x = 110;
        assert (c.pick (p, 0.28));
        p.x = 170;
        assert (!c.pick (p, 0.28));
        // fill:none
        c.style.value = "stroke: white; stroke-width: 1mm; fill: none";
        p.x = 100;
        p.y = 100;
        assert (!c.pick (p, 0.28));
        p.x = 100;
        p.y = 20;
        assert (!c.pick (p, 0.28));
        p.x = 100;
        p.y = 10;
        assert (c.pick (p, 0.28));
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/element/pick/line",
    ()=>{
      try {
        var d = new GSvg.Document ();
        d.root_element.pixel_unit_to_millimeter_x = 0.1;
        d.root_element.pixel_unit_to_millimeter_y = 0.1;
        assert (d.root_element.pixel_unit_to_millimeter_x == 0.1);
        assert (d.root_element.pixel_unit_to_millimeter_y == 0.1);
        var r = d.root_element;
        var p = new Point ();
        p.x = 0;
        p.y = 0;
        // Filled
        var e = r.create_line ("0mm", "0mm", "10mm", "10mm", "stroke: white; stroke-width: 1mm");
        r.append_child (e);
        assert (e.is_filled ());
        var l = e.stroke_width_to_lenght ();
        assert (l.value == 1);
        assert (l.unit_type == DomLength.Type.MM);
        assert (r.convert_length_x_pixels (l) == 10.0);
        assert (e.pick (p, 10));
        p.x = 50;
        p.y = 50;
        assert (e.pick (p, 10));
        p.x = 100;
        p.y = 150;
        assert (!e.pick (p, 10));
        p.x = 10;
        p.y = 0;
        assert (e.pick (p, 10));
        p.x = 0;
        p.y = 10;
        assert (e.pick (p, 10));
        p.x = 20;
        p.y = 0;
        assert (!e.pick (p, 10));
        p.x = 0;
        p.y = 20;
        assert (!e.pick (p, 10));
      } catch (GLib.Error er) {
        warning ("Error: %s", er.message);
      }
    });
    Test.add_func ("/gsvg/element/pick/rect",
    ()=>{
      try {
        var d = new GSvg.Document ();
        d.root_element.pixel_unit_to_millimeter_x = 0.1;
        d.root_element.pixel_unit_to_millimeter_y = 0.1;
        assert (d.root_element.pixel_unit_to_millimeter_x == 0.1);
        assert (d.root_element.pixel_unit_to_millimeter_y == 0.1);
        var r = d.root_element;
        var p = new Point ();
        p.x = 0;
        p.y = 0;
        // Filled
        var e = r.create_rect ("0mm", "0mm", "10mm", "10mm", null, null, "stroke: white; stroke-width: 1mm");
        r.append_child (e);
        assert (e.is_filled ());
        var l = e.stroke_width_to_lenght ();
        assert (l.value == 1);
        assert (l.unit_type == DomLength.Type.MM);
        assert (r.convert_length_x_pixels (l) == 10.0);
        assert (e.pick (p, 10));
        p.x = 50;
        p.y = 50;
        assert (e.pick (p, 10));
        p.x = 100;
        p.y = 100;
        assert (e.pick (p, 10));
        p.x = 0;
        p.y = 110;
        assert (e.pick (p, 10));
        p.x = 110;
        p.y = 0;
        assert (e.pick (p, 10));
        p.x = 110;
        p.y = 110;
        assert (e.pick (p, 10));
        p.x = 120;
        p.y = 0;
        assert (!e.pick (p, 10));
        p.x = 0;
        p.y = 120;
        assert (!e.pick (p, 10));
        // Not Filled
        e.style.value = "stroke: white; stroke-width: 1mm; fill: none";
        p.x = 50;
        p.y = 50;
        assert (!e.pick (p, 10));
      } catch (GLib.Error er) {
        warning ("Error: %s", er.message);
      }
    });
    Test.add_func ("/gsvg/element/pick/ellipse",
    ()=>{
      try {
        var d = new GSvg.Document ();
        d.root_element.pixel_unit_to_millimeter_x = 0.1;
        d.root_element.pixel_unit_to_millimeter_y = 0.1;
        assert (d.root_element.pixel_unit_to_millimeter_x == 0.1);
        assert (d.root_element.pixel_unit_to_millimeter_y == 0.1);
        var r = d.root_element;
        var p = new Point ();
        p.x = 0;
        p.y = 0;
        // Filled
        var e = r.create_ellipse ("10mm", "10mm", "10mm", "5mm", "stroke: white; stroke-width: 1mm");
        r.append_child (e);
        assert (e.is_filled ());
        var l = e.stroke_width_to_lenght ();
        assert (l.value == 1);
        assert (l.unit_type == DomLength.Type.MM);
        message ("%g", r.convert_length_x_pixels (l));
        assert (r.convert_length_x_pixels (l) == 1/0.1);
        p.x = 0;
        p.y = 0;
        assert (!e.pick (p, 10));
        p.x = 100;
        p.y = 100;
        assert (e.pick (p, 10));
        p.x = 100;
        p.y = 50;
        assert (e.pick (p, 10));
        p.x = 100;
        p.y = 60;
        assert (e.pick (p, 10));
        p.x = 0;
        p.y = 100;
        assert (e.pick (p, 10));
        p.x = 10;
        p.y = 10;
        assert (!e.pick (p, 10));
        p.x = 120;
        p.y = 0;
        assert (!e.pick (p, 10));
        p.x = 0;
        p.y = 40;
        assert (!e.pick (p, 10));
        // Not Filled
        e.style.value = "stroke: white; stroke-width: 1mm; fill: none";
        p.x = 100;
        p.y = 100;
        assert (!e.pick (p, 10));
      } catch (GLib.Error er) {
        warning ("Error: %s", er.message);
      }
    });
    return Test.run ();
  }
}
