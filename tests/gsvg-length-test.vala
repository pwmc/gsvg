/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * gsvg-length-test.vala

 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;

class GSvgTest.Suite : GLib.Object
{
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/gsvg/length/length",
    ()=>{
      var l = new GSvg.Length ();
      l.parse ("1mm");
      assert (l.value == 1.0);
      assert (l.unit_type == DomLength.Type.MM);
      l.value = 10.0;
      assert (l.to_string() == "10mm");
      assert (l.value_as_string == "10mm");
      l.unit_type = DomLength.Type.IN;
      assert (l.to_string () == "10in");
      assert (l.value_as_string == "10in");
      l = new Length ();
      assert (l.to_string () == "0");
      l.value = 3.0;
      assert (l.value_as_string == "3");
      l.unit_type = DomLength.Type.IN;
      assert (l.value_as_string == "3in");
    });
    Test.add_func ("/gsvg/length/animated",
    ()=>{
      var l = new GSvg.AnimatedLength ();
      assert (l.value == "0");
      l.value = "1mm";
      assert (l.base_val.value == 1.0);
      assert (l.base_val.unit_type == DomLength.Type.MM);
      l.base_val.value = 10.0;
      assert (l.value == "10mm");
      l.base_val.value = 3.0;
      l.base_val.unit_type = DomLength.Type.IN;
      message (l.value);
      assert (l.value == "3in");
      l = new AnimatedLength ();
      assert (l.value == "0");
    });
    Test.add_func ("/gsvg/length/convert",
    ()=>{
      var l = new GSvg.AnimatedLength ();
      assert (l.value == "0");
      l.value = "1mm";
      assert (l.base_val.value == 1.0);
      assert (l.base_val.unit_type == DomLength.Type.MM);
      double res = l.base_val.value_to_specified_units (DomLength.Type.MM, 96);
      assert (res == 1);
      l.base_val.value = 25.4;
      assert (l.value == "25.4mm");
      res = l.base_val.value_to_specified_units (DomLength.Type.IN, 96);
      assert (res == 1);
      res = l.base_val.value_to_specified_units (DomLength.Type.PX, 96);
      assert (res == 96);
    });
    return Test.run ();
  }
}
