/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * gsvg-css-parsing-test.vala

 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using GXml;

class GSvgTest.Suite : GLib.Object
{
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/gsvg/css/parse",
    ()=>{
      var style = "background: black; stroke: blue; stroke-width: 3mm";
      var sd = new CssStyleDeclaration ();
      sd.value = style;
      assert (sd is GXml.Property);
      assert (sd.value != null);
      assert (sd.value == style);
      assert (sd.length == 3);
      message ("Getting items");
      for (int i = 0; i < sd.length; i++) {
        message ("%s", sd.item (i));
      }
      assert (sd.item (0) != null);
      assert (sd.item (0) == "background");
      assert (sd.item (1) == "stroke");
      assert (sd.item (2) == "stroke-width");
      assert (sd.get_property_value ("stroke-width") == "3mm");
    });
    Test.add_func ("/gsvg/element/style/properties",
    ()=>{
      try {
        var e = new LineElement () as GSvg.DomLineElement;
        e.set_attribute ("style", "stroke-width: 30; stroke: blue");
        assert (e.style != null);
        assert (e.style.value == "stroke-width: 30; stroke: blue");
        var v = e.get_style_property_value ("stroke-width");
        assert (v != null);
        assert (v == "30");
        ((DomStylable) e).style.value = "stroke: blue";
        v = e.get_style_property_value ("stroke");
        assert (v != null);
        assert (v == "blue");
      } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    });
    Test.add_func ("/gsvg/element/style/set",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        var r = svg.create_rect ("0mm","0mm","50mm","50mm", null, null);
        r.set_attribute ("style", "stroke-width:1mm");
        assert (((GXml.Object) r).get_attribute ("style") != null);
        assert (r.get_attribute ("style") != null);
        assert (r.get_attribute ("style") == "stroke-width:1mm");
        assert (r.style != null);
        message (r.write_string ());
        message (r.style.value);
        assert (r.style.value == "stroke-width:1mm");
        svg.append_child (r);
      } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    });
    Test.add_func ("/gsvg/element/style/serialization",
    ()=>{
      try {
        var l = new GSvg.TextElement ();
        l.read_from_string ("""<text style="stroke: black" x="10mm" y="20mm">Some Text</text>""");
        message (l.write_string ());
        assert (l.style != null);
        assert (l.get_attribute ("style") != null);
        assert (l.get_attribute ("style") == "stroke: black");
        message (l.style.value);
        assert (l.style.value == "stroke: black");
      } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    });
    Test.add_func ("/gsvg/element/style/serialization/invalidate",
    ()=>{
      try {
        var l = new GSvg.TextElement ();
        l.read_from_string ("""<text style="e:stroke: black" x="10mm" y="20mm">Some Text</text>""");
        message (l.write_string ());
        assert (l.style != null);
        assert (l.get_attribute ("style") != null);
        assert (l.get_attribute ("style") == "e:stroke: black");
        message (l.style.value);
        assert (l.style.value == "e:stroke: black");
      } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    });
    return Test.run ();
  }
}
