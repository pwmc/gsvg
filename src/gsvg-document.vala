/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016,2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using Gee;

/**
 * Creates an stand alone SVG document, without a top level 'svg' element.
 *
 * You should use {@link add_svg} to add a new 'svg' node.
 */
public class GSvg.Document : GXml.Document,  GSvg.DomDocument {
  protected string _referrer = "";
  protected string _domain = "";
  public string title {
    owned get {
      if (document_element == null) return "";
      if (document_element.local_name.down () != "svg") return "";
      var els = document_element.get_elements_by_tag_name ("title");
      if (els.length == 0) return "";
      return els.item (0).text_content;
    }
  }
  public string referrer {
    get { return _referrer; }
  }
  public string domain { get { return _domain; } }
  public string url { get { return _url; } }
  public DomSvgElement? root_element {
    owned get {
      return mroot_element;
    }
  }
  [Description (nick="::ROOT")]
  public SvgElement mroot_element { get; set; }
  construct {
    var dt = new GXml.DocumentType (this, "svg", "-//W3C//DTD SVG 1.1//EN",
  "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd");
    try {
      append_child (dt);
      mroot_element = create_svg (null, null, null, null) as SvgElement;
      append_child (mroot_element);
    } catch (GLib.Error e) { warning ("Error: "+e.message); }
  }
  public DomSvgElement add_svg (string? x,
                            string? y,
                            string? width,
                            string? height,
                            string? viewbox = null,
                            string? title = null,
                            string? desc = null)
  {
    if (root_element != null) {
      warning ("You should add just one SVG element per document");
      return root_element;
    }
    GXml.DomNode p = this;
    if (root_element != null) {
      p = root_element;
    }
    var s = add_svg_internal (p, x, y, width, height, viewbox, title, desc);
    mroot_element = s as SvgElement;
    return s;
  }
  public DomSvgElement create_svg (string? x,
                            string? y,
                            string? width,
                            string? height,
                            string? viewbox = null,
                            string? title = null,
                            string? desc = null)
  {
    return create_svg_internal (this, x, y, width, height, viewbox, title, desc);
  }
  internal static DomSvgElement add_svg_internal (GXml.DomNode parent,
                            string? x,
                            string? y,
                            string? width,
                            string? height,
                            string? viewbox = null,
                            string? title = null,
                            string? desc = null)
  {
    var s = create_svg_internal (parent.owner_document, x, y, width, height, viewbox, title, desc);
    try {
      parent.append_child (s);
      DomSvgElement root = null;
      if (parent is DomSvgElement) {
        root = parent as DomSvgElement;
      }
      if (parent is GSvg.DomDocument) {
        var doc = parent as GSvg.DomDocument;
        if (doc.root_element != null) {
          root = doc.root_element;
        }
      }
      if (root != null) {
        s.pixel_unit_to_millimeter_x = root.pixel_unit_to_millimeter_x;
        s.pixel_unit_to_millimeter_y = root.pixel_unit_to_millimeter_y;
        s.screen_pixel_to_millimeter_x = root.screen_pixel_to_millimeter_x;
        s.screen_pixel_to_millimeter_y = root.screen_pixel_to_millimeter_y;
      }
    } catch (GLib.Error e) { warning ("Error: "+e.message); }

    return s;
  }
  internal static DomSvgElement create_svg_internal (GXml.DomDocument doc,
                            string? x,
                            string? y,
                            string? width,
                            string? height,
                            string? viewbox = null,
                            string? title = null,
                            string? desc = null)
  {

    DomSvgElement nsvg = null;
    try {
      nsvg = GLib.Object.new (typeof (GSvg.SvgElement),
                          "owner-document", doc) as DomSvgElement;
      if (x != null) {
        nsvg.set_attribute ("x", x);
      }
      if (y != null) {
        nsvg.set_attribute ("y", y);
      }
      if (width != null) {
        nsvg.set_attribute ("width", width);
      }
      if (height != null) {
        nsvg.set_attribute ("height", height);
        }
      if (viewbox != null) {
        nsvg.set_attribute ("viewbox", viewbox);
      }
      if (title != null) {
        nsvg.add_title (title);
      }
      if (desc != null) {
        nsvg.add_desc (desc);
      }
    } catch (GLib.Error e) {
      warning ("Error while creating SVG: %s", e.message);
    }
    return nsvg;
  }
}
