/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016,2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using Gee;

public class GSvg.Rect : GLib.Object, GXml.Property, DomRect {
  public double x { get; set; }
  public double y { get; set; }
  public double width { get; set; }
  public double height { get; set; }

  public string? value {
    owned get { return to_string (); }
    set { parse (value); }
  }

  public bool validate_value (string? str) {
    return double.try_parse (str);
  }

  public void parse (string str) {
    if (!(" " in str)) {
      x = double.parse (str);
      return;
    }
    string[] st = str.split (" ");
    for (int i = 0; i < st.length; i++) {
      if (i == 0) x = double.parse (st[0]);
      if (i == 1) y = double.parse (st[1]);
      if (i == 2) width = double.parse (st[2]);
      if (i == 3) height = double.parse (st[3]);
    }
  }
  public string to_string () {
    string t = "";
    t += "%g ".printf (x);
    t += "%g ".printf (y);
    t += "%g ".printf(width);
    t += "%g".printf(height);
    return t;
  }
}

public class GSvg.AnimatedRect : GLib.Object,
                                GXml.Property,
                                DomAnimatedRect
{
  private DomRect _base_val;
  private DomRect _anim_val;
  public DomRect base_val {
    get {
      if (_base_val == null) _base_val = new Rect ();
      return _base_val;
    }
  }
  public DomRect anim_val {
    get {
      if (_anim_val == null) _anim_val = new Rect ();
      return _anim_val;
    }
  }
  public string? value {
    owned get { return base_val.to_string (); }
    set { base_val.parse (value); }
  }
  public bool validate_value (string? str) {
    return _base_val.validate_value (str); // FIXME:
  }
}


public class GSvg.PreserveAspectRatio : GLib.Object, DomPreserveAspectRatio {
  private string defer = null;
  public DomPreserveAspectRatio.Type align { get; set; }
  public DomPreserveAspectRatio.MeetorSlice meet_or_slice { get; set; }
  public string? to_string () {
    if (DomPreserveAspectRatio.Type.to_string (align) == null) return null;
    string str = "";
    if (defer != null)
      str += defer+" ";
    str += DomPreserveAspectRatio.Type.to_string (align);
    if (meet_or_slice != DomPreserveAspectRatio.MeetorSlice.UNKNOWN)
      str += " "+DomPreserveAspectRatio.MeetorSlice.to_string (meet_or_slice);
    return str;
  }
  public void parse (string str) {
    if (str == "") return;
    string a = str;
    string m = "";
    if (" " in str) {
      string[] st = str.split (" ");
      if (st.length == 0) return;
      int p = 0;
      if ("defer" == st[0].down ()) {
        defer = st[0];
        p++;
      }
      a = st[p];
      if (p+1 < st.length)
        p++;
      m = st[p];
    }
    align = DomPreserveAspectRatio.Type.parse (a);
    meet_or_slice = DomPreserveAspectRatio.MeetorSlice.parse (m);
  }
}

public class GSvg.AnimatedPreserveAspectRatio : GLib.Object, GXml.Property, DomAnimatedPreserveAspectRatio {
  public DomPreserveAspectRatio base_val { get; set; }
  public DomPreserveAspectRatio anim_val { get; set; }
  public string? value {
    owned get {
      if (base_val == null)
        return null;
      return ((PreserveAspectRatio) base_val).to_string ();
    }
    set {
      if (base_val == null)
        base_val = new PreserveAspectRatio () as DomPreserveAspectRatio;
      ((PreserveAspectRatio) base_val).parse (value);
    }
  }
  public bool validate_value (string? val) {
    return true; // FIXME
  }
}

public class GSvg.DescriptiveElement : GSvg.Stylable,
                                        DomLangSpace
{
  // LangSpace
  [Description (nick="::xml:lang")]
  public string xmllang { get; set; }
  [Description (nick="::xml:space")]
  public string xmlspace { get; set; }
}

public class GSvg.TitleElement : GSvg.DescriptiveElement, DomTitleElement {
  construct { initialize ("title"); }
}

public class GSvg.DescElement : GSvg.DescriptiveElement, DomDescElement {
  construct { initialize ("desc"); }
}

public class GSvg.MetadataElement : GSvg.Element, DomMetadataElement {
  construct { initialize ("metadata"); }
}

