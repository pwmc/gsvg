/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-gs-radial-gradient-element.vala
 *
 * Copyright (C) 2016, 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

public class GSvg.RadialGradientElement : GradientElement,
                           GSvg.DomRadialGradientElement, GXml.MappeableElement
{
  private AnimatedLength _mcx;
  public DomAnimatedLength cx { get { return _mcx as DomAnimatedLength; } }
  [Description (nick="::cx")]
  public AnimatedLength mcx {
    get { return _mcx; }
    set { _mcx = value; }
  }
  private AnimatedLength _mcy;
  public DomAnimatedLength cy { get { return _mcy as DomAnimatedLength; }  }
  [Description (nick="::cy")]
  public AnimatedLength mcy {
    get { return _mcy; }
    set { _mcy = value; }
  }
  private AnimatedLength _mr;
  public DomAnimatedLength r { get { return _mr as DomAnimatedLength; } }
  [Description (nick="::r")]
  public AnimatedLength mr {
    get { return _mr; }
    set { _mr = value; }
  }
  private AnimatedLength _mfx;
  public DomAnimatedLength fx { get { return _mfx as DomAnimatedLength; } }
  [Description (nick="::fx")]
  public AnimatedLength mfx {
    get { return _mfx; }
    set { _mfx = value; }
  }
  private AnimatedLength _mfy;
  public DomAnimatedLength fy { get { return _mfy as DomAnimatedLength; } }
  [Description (nick="::fy")]
  public AnimatedLength mfy {
    get { return _mfy; }
    set { _mfy = value; }
  }
  construct {
    initialize ("radialGradient");
  }
  // MappeableElement
  public string get_map_key () { return id; }
}
