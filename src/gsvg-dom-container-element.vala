/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-interfaces.vala
 *
 * Copyright (C) 2016-2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using GXml;

public interface GSvg.DomContainerElement : GLib.Object {
  // Shapes access
  public abstract DomSvgElementMap svgs { get; }
  public abstract DomGElementMap groups { get; }
  public abstract DomRectElementMap rects { get; }
  public abstract DomCircleElementMap circles { get; }
  public abstract DomEllipseElementMap ellipses { get; }
  public abstract DomLineElementMap lines { get; }
  public abstract DomPolylineElementMap polylines { get; }
  public abstract DomPolygonElementMap polygons { get; }
  public abstract DomTextElementMap texts { get; }
  public abstract DomPathElementMap paths { get; }
  public abstract DomImageElementMap images { get; }
  public abstract DomUseElementMap uses { get; }
  public abstract DomSwitchElementMap switches { get; }
  public abstract DomSymbolElementMap symbols { get; }
  public abstract DomClipPathElementMap clip_paths { get; }
  public abstract DomScriptElementMap scripts { get; }
  public abstract DomAnimateElementMap animates { get; }
  public abstract DomSetElementMap sets { get; }
  public abstract DomAnimateMotionElementMap animate_motions { get; }
  public abstract DomAnimateColorElementMap animate_colors { get; }
  public abstract DomAnimateTransformElementMap animate_transformations { get; }
  public abstract DomLinearGradientElementMap linear_gradients { get; }
  public abstract DomRadialGradientElementMap radial_gradients { get; }
  public abstract DomDefsElementMap defs { get; }
}

