/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016, 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

public class GSvg.LineElement : GSvg.Transformable,
                           GSvg.DomLineElement, GXml.MappeableElement
{
  public DomAnimatedLengthX x1 { get; set; }
  [Description (nick="::x1")]
  public AnimatedLengthX mx1 {
    get { return x1 as AnimatedLengthX; }
    set { x1 = value as DomAnimatedLengthX; }
  }
  public DomAnimatedLengthY y1 { get; set; }
  [Description (nick="::y1")]
  public AnimatedLengthY my1 {
    get { return y1 as AnimatedLengthY; }
    set { y1 = value as DomAnimatedLengthY; }
  }
  public DomAnimatedLengthX x2 { get; set; }
  [Description (nick="::x2")]
  public AnimatedLengthX mx2 {
    get { return x2 as AnimatedLengthX; }
    set { x2 = value as DomAnimatedLengthX; }
  }
  public DomAnimatedLengthY y2 { get; set; }
  [Description (nick="::y2")]
  public AnimatedLengthY my2 {
    get { return y2 as AnimatedLengthY; }
    set { y2 = value as DomAnimatedLengthY; }
  }
  construct {
    initialize ("line");
  }
  // MappeableElement
  public string get_map_key () { return id; }

  public override bool pick (DomPoint point, double delta) {
    if (x1 == null || y1 == null || x2 == null || y2 == null) {
      return false;
    }
    if (x1.base_val == null || y1.base_val == null
        || x2.base_val == null || y2.base_val == null) {
      return false;
    }
    double m = 0;
    double p = -1;
    double mx, my, px, py, cstroke;
    mx =my = px = py = cstroke = 0.0;
    var lstroke = stroke_width_to_lenght ();
    if (owner_document is GSvg.DomDocument) {
      var tsvg = ((GSvg.DomDocument) owner_document).root_element;
      if (tsvg != null) {
        mx = tsvg.convert_length_x_pixels (x2.base_val) - tsvg.convert_length_x_pixels(x1.base_val);
        my = tsvg.convert_length_y_pixels (y2.base_val) - tsvg.convert_length_y_pixels (y1.base_val);
        px = point.x - tsvg.convert_length_x_pixels (x1.base_val);
        py = point.y - tsvg.convert_length_y_pixels (y1.base_val);
        cstroke = tsvg.convert_length_x_pixels (lstroke);
      }
    } else {
      mx = x2.base_val.value - x1.base_val.value;
      my = y2.base_val.value - y1.base_val.value;
      px = point.x - x1.base_val.value;
      px = point.y - y1.base_val.value;
      cstroke = lstroke.value;
    }
    if (mx != 0) {
      m = my / mx;
      p = m * px - py;
    } else {
      p = px;
    }
    if (Math.fabs (p) <= delta) {
      return true;
    }
    if ((Math.fabs (p) + cstroke / 2) <= delta) {
      return true;
    }
    return false;
  }
}

