/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* css-classes.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GLib;
using Gee;

public class GSvg.Number : GLib.Object, DomNumber {
  public double value { get; set; }
}

public class GSvg.AnimatedNumber : GLib.Object, GXml.Property, DomAnimatedNumber {
  private string _val = "0";
  private double _base_val = 0.0;
  public double base_val {
    get {
      return _base_val;
    }
    set {
      _base_val = value;
      _val = DomPoint.double_to_string (_base_val);
    }
  }
  public double anim_val  { get; set; }

  // GomProperty
  public string? value {
    set {
      _val = value;
    }
    owned get {
      return _val;
    }
  }
  public bool validate_value (string? val) { return true; }
}

public class GSvg.NumberList : Gee.ArrayList<DomNumber>,
                          DomNumberList
{
  private string separator = " ";
  public int number_of_items { get { return size; } }

  public new void  clear () throws GLib.Error { ((Gee.ArrayList<DomNumber>) this).clear (); }
  public new DomNumber initialize (DomNumber new_item) throws GLib.Error {
    add (new_item);
    return new_item;
  }
  public DomNumber get_item (int index) throws GLib.Error {
    return get (index);
  }
  public DomNumber insert_item_before (DomNumber new_item, int index) throws GLib.Error {
    insert (index, new_item);
    return new_item;
  }
  public DomNumber replace_item (DomNumber new_item, int index) throws GLib.Error {
    remove_at (index);
    insert (index, new_item);
    return new_item;
  }
  public DomNumber remove_item (int index) throws GLib.Error {
    return remove_at (index);
  }
  public DomNumber append_item (DomNumber new_item) throws GLib.Error {
    add (new_item);
    return new_item;
  }
  public string? value {
    set {
      if (" " in value) separator = " ";
      if ("," in value) separator = ",";
      string[] tks = value.split (separator);
      for (int i = 0; i < tks.length; i++) {
        var p = new Number ();
        p.value = double.parse (tks[i]);
        add (p as DomNumber);
      }
    }
    owned get {
      if (size == 0) return null;
      string str = "";
      for (int i = 0; i < size; i++) {
        var p = get (i);
        str += "%0.0g".printf (p.value);
        if (i+1 < size) str += separator;
      }
      return str;
    }
  }
}

public class GSvg.AnimatedNumberList : GLib.Object,
                                GXml.Property,
                                DomAnimatedNumberList
{
  private DomNumberList _base_val;
  private DomNumberList _anim_val;
  public DomNumberList base_val {
    get {
      if (_base_val == null) _base_val = new NumberList ();
      return _base_val;
    }
    set { _base_val = value; }
  }
  public DomNumberList anim_val {
    get {
      if (_anim_val == null) _anim_val = new NumberList ();
      return _anim_val;
    }
    set { _anim_val = value; }
  }
  // GomProperty
  public string? value {
    owned get {
      if (_base_val == null) return null;
      return base_val.value;
    }
    set {
      if (_base_val == null) _base_val = new NumberList ();
      base_val.value = value;
    }
  }
  public bool validate_value (string? val) {
    return "," in val || " " in val; // FIXME
  }
}

public class GSvg.AnimatedEnumeration : GLib.Object, DomAnimatedEnumeration {
  public int base_val { get; set; }
  public int anim_val { get; set; }
}
