/* gsvg-dom-interfaces-painting.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

namespace GSvg {

public interface DomPaint : GLib.Object, DomColor {

  public abstract uint paintType { get; }
  public abstract string uri { get; }

  public abstract void setUri(string uri);
  public abstract void setPaint(uint paintType,
                                string uri,
                                string rgbColor,
                                string iccColor) throws GLib.Error;

  /**
   * Paint Types
  */
  public enum Type {
    UNKNOWN = 0,
    RGBCOLOR = 1,
    RGBCOLOR_ICCCOLOR = 2,
    NONE = 101,
    CURRENTCOLOR = 102,
    URI_NONE = 103,
    URI_CURRENTCOLOR = 104,
    URI_RGBCOLOR = 105,
    URI_RGBCOLOR_ICCCOLOR = 106,
    URI = 107
  }
}

public interface DomMarkerElement : GLib.Object,
                             DomElement,
                             DomLangSpace,
                             DomExternalResourcesRequired,
                             DomStylable,
                             DomFitToViewBox {

  public abstract  DomAnimatedLength ref_x { get; }
  public abstract  DomAnimatedLength ref_y { get;}
  public abstract  DomAnimatedEnumeration marker_units { get;}
  public abstract  DomAnimatedLength marker_width { get; }
  public abstract  DomAnimatedLength marker_height { get; }
  public abstract  DomAnimatedEnumeration orient_type { get; }
  public abstract  DomAnimatedAngle orient_angle { get; }

  public abstract void set_orient_to_auto () throws GLib.Error;
  public abstract void set_orient_to_angle (DomAngle angle) throws GLib.Error;
}

  // Marker Unit Types
public enum MarkerUnits {
  UNKNOWN = 0,
  USERSPACEONUSE = 1,
  STROKEWIDTH = 2
}

  // Marker Orientation Types
public enum MarkerOrient {
  UNKNOWN = 0,
  AUTO = 1,
  ANGLE = 2
}

public interface DomColorProfileElement : GLib.Object,
                                   DomElement,
                                   DomURIReference {
  public abstract string local { get; set; }
  public abstract string name { get; set; }
  public abstract uint rendering_intent { get; set; }
}

public interface DomColorProfileRule : GLib.Object, CSSRule {
  public abstract string src { get; set; }
  public abstract string name { get; set; }
  public abstract uint rendering_intent { get; set; }
}

  // Spread Method Types
public enum SpreadMethod {
  UNKNOWN = 0,
  PAD = 1,
  REFLECT = 2,
  REPEAT = 3
}

public interface DomStopElement : GLib.Object,
                           DomElement,
                           DomStylable {
  public abstract  DomAnimatedNumber offset { get; }
}

public interface DomPatternElement : GLib.Object,
                              DomElement,
                              DomURIReference,
                              DomTests,
                              DomLangSpace,
                              DomExternalResourcesRequired,
                              DomStylable,
                              DomFitToViewBox {
  public abstract  DomAnimatedEnumeration pattern_units { get; }
  public abstract  DomAnimatedEnumeration pattern_content_units { get; }
  public abstract  DomAnimatedTransformList pattern_transform { get; }
  public abstract  DomAnimatedLength x { get; }
  public abstract  DomAnimatedLength y { get; }
  public abstract  DomAnimatedLength width { get; }
  public abstract  DomAnimatedLength height { get; }
}

public interface DomMaskElement : GLib.Object,
                           DomElement,
                           DomTests,
                           DomLangSpace,
                           DomExternalResourcesRequired,
                           DomStylable {
  public abstract  DomAnimatedEnumeration mask_units { get; }
  public abstract  DomAnimatedEnumeration mask_content_units { get; }
  public abstract  DomAnimatedLength x { get; }
  public abstract  DomAnimatedLength y { get; }
  public abstract  DomAnimatedLength width { get; }
  public abstract  DomAnimatedLength height { get; }
}

} // GSvg
