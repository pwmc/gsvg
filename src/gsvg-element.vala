/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016,2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using Gee;


public class GSvg.Element : GSvg.Object,
                        GSvg.DomElement
{
  protected DomSvgElement _owner_svg_element;
  protected GSvg.DomElement _viewport_element;
  // Element
  [Description (nick="::xml:base")]
  public string xmlbase { get; set; }
  public DomSvgElement? owner_svg_element {
    get { return _owner_svg_element; }
  }
  public GSvg.DomElement? viewport_element { get { return _viewport_element; } }
  // Styling properties
  // Fonts properties
  [Description (nick="::font")]
  public string font { get; set; }
  [Description (nick="::font-family")]
  public string font_family { get; set; }
  [Description (nick="::font-size")]
  public string font_size { get; set; }
  [Description (nick="::font-size-adjust")]
  public string font_size_adjust { get; set; }
  [Description (nick="::font-stretch")]
  public string font_stretch { get; set; }
  [Description (nick="::font-style")]
  public string font_style { get; set; }
  [Description (nick="::font-variant")]
  public string font_variant { get; set; }
  [Description (nick="::font-weight")]
  public string font_weight { get; set; }
  // Text properties
  [Description (nick="::direction")]
  public string direction { get; set; }
  [Description (nick="::letter-spacing")]
  public string letter_spacing { get; set; }
  [Description (nick="::text-decoration")]
  public string text_decoration { get; set; }
  [Description (nick="::unicode-bidi")]
  public string unicode_bidi { get; set; }
  [Description (nick="::word-spacing")]
  public string word_spacing { get; set; }
  // Other visual properties
  [Description (nick="::clip")]
  public string clip { get; set; }
  [Description (nick="::color")]
  public string color { get; set; }
  [Description (nick="::cursor")]
  public string cursor { get; set; }
  [Description (nick="::display")]
  public string display { get; set; }
  [Description (nick="::overflow")]
  public string overflow { get; set; }
  [Description (nick="::visibility")]
  public string visibility { get; set; }
  // Clipping, Masking and Compositing properties
  [Description (nick="::clip-path")]
  public string clip_path { get; set; }
  [Description (nick="::clip-rule")]
  public string clip_rule { get; set; }
  [Description (nick="::mask")]
  public string mask { get; set; }
  [Description (nick="::opacity")]
  public string opacity { get; set; }
  // Filter Effects properties
  [Description (nick="::enable-background")]
  public string enable_background { get; set; }
  [Description (nick="::filter")]
  public string filter { get; set; }
  [Description (nick="::flood-color")]
  public string flood_color { get; set; }
  [Description (nick="::flood-opacity")]
  public string flood_opacity { get; set; }
  [Description (nick="::lighting-color")]
  public string lighting_color { get; set; }
  // Gradient properties
  [Description (nick="::stop-color")]
  public string stop_color { get; set; }
  [Description (nick="::stop-opacity")]
  public string stop_opacity { get; set; }
  // Interactivity properties
  [Description (nick="::pointer-events")]
  public string pointer_events { get; set; }
  // Color and painting properties
  [Description (nick="::color-interpolation")]
  public string color_interpolation { get; set; }
  [Description (nick="::color-interpolation-filter")]
  public string color_interpolation_filter { get; set; }
  [Description (nick="::color-profile")]
  public string color_profile { get; set; }
  [Description (nick="::color-rendering")]
  public string color_rendering { get; set; }
  [Description (nick="::fill")]
  public string fill { get; set; }
  [Description (nick="::fill-opacity")]
  public string fill_opacity { get; set; }
  [Description (nick="::fill-rule")]
  public string fill_rule { get; set; }
  [Description (nick="::image-rendering")]
  public string image_rendering { get; set; }
  [Description (nick="::marker")]
  public string marker { get; set; }
  [Description (nick="::maker-end")]
  public string maker_end { get; set; }
  [Description (nick="::marker_mid")]
  public string maker_mid { get; set; }
  [Description (nick="::maker-start")]
  public string maker_start { get; set; }
  [Description (nick="::shape-rendering")]
  public string shape_rendering { get; set; }
  [Description (nick="::stroke")]
  public string stroke { get; set; }
  [Description (nick="::stroke-dasharray")]
  public string stroke_dasharray { get; set; }
  [Description (nick="::stroke-dashoffset")]
  public string stroke_dashoffset { get; set; }
  [Description (nick="::stroke-linecap")]
  public string stroke_linecap { get; set; }
  [Description (nick="::stroke-linejoin")]
  public string stroke_linejoin { get; set; }
  [Description (nick="::stroke-miterlimit")]
  public string stroke_miterlimit { get; set; }
  [Description (nick="::stroke-opacity")]
  public string stroke_opacity { get; set; }
  [Description (nick="::stroke-width")]
  public string stroke_width { get; set; }
  [Description (nick="::text-rendering")]
  public string text_rendering { get; set; }
  // Other text properties
  [Description (nick="::alignment-baseline")]
  public string alignment_baseline { get; set; }
  [Description (nick="::baseline-shift")]
  public string baseline_shift { get; set; }
  [Description (nick="::dominant-baseline")]
  public string dominant_baseline { get; set; }
  [Description (nick="::glyph-orientation-horizontal")]
  public string glyph_orientation_horizontal { get; set; }
  [Description (nick="::glyph-orientation-vertical")]
  public string glyph_orientation_vertical { get; set; }
  [Description (nick="::kerning")]
  public string kerning { get; set; }
  [Description (nick="::text-anchor")]
  public string text_anchor { get; set; }
  [Description (nick="::writing-mode")]
  public string writing_mode { get; set; }

  public void GSvg.DomElement.read_from_file (GLib.File file) throws GLib.Error {
    ((GXml.Element) this).read_from_file (file);
  }
  public void GSvg.DomElement.read_from_uri (string uri) throws GLib.Error {
    ((GXml.Element) this).read_from_uri (uri);
  }
  public void GSvg.DomElement.read_from_string (string str) throws GLib.Error {
    ((GXml.Element) this).read_from_string (str);
  }
  public void GSvg.DomElement.read_from_stream (GLib.InputStream istream, GLib.Cancellable? cancellable = null) throws GLib.Error {
    ((GXml.Element) this).read_from_stream (istream, cancellable);
  }
  public string GSvg.DomElement.write_string () throws GLib.Error {
    return ((GXml.Element) this).write_string ();
  }
  public void GSvg.DomElement.write_file (GLib.File file) throws GLib.Error {
    ((GXml.Element) this).write_file (file);
  }
  public void GSvg.DomElement.write_stream (OutputStream stream) throws GLib.Error {
    ((GXml.Element) this).write_stream (stream);
  }

  public async void GSvg.DomElement.read_from_file_async (GLib.File file, Cancellable? cancellable = null) throws GLib.Error {
    yield ((GXml.Element) this).read_from_file_async (file, cancellable);
  }
  public async void GSvg.DomElement.read_from_uri_async (string uri, Cancellable? cancellable = null) throws GLib.Error {
    ((GXml.Element) this).read_from_uri (uri);
  }
  public async void GSvg.DomElement.read_from_string_async (string str, Cancellable? cancellable = null) throws GLib.Error {
    yield ((GXml.Element) this).read_from_string_async (str);
  }
  public async void GSvg.DomElement.read_from_stream_async (InputStream istream, Cancellable? cancellable = null) throws GLib.Error {
    yield ((GXml.Element) this).read_from_stream_async (istream, cancellable);
  }
  public async string GSvg.DomElement.write_string_async (Cancellable? cancellable = null) throws GLib.Error {
    return yield ((GXml.Element) this).write_string_async ();
  }
  public async void GSvg.DomElement.write_file_async (GLib.File file, Cancellable? cancellable = null) throws GLib.Error {
    yield ((GXml.Element) this).write_file_async (file);
  }
  public async void GSvg.DomElement.write_stream_async (OutputStream stream, Cancellable? cancellable = null) throws GLib.Error {
    yield ((GXml.Element) this).write_stream_async (stream);
  }
  public InputStream GSvg.DomElement.read () throws GLib.Error {
    return ((GXml.Element) this).create_stream ();
  }

  public virtual bool pick (GSvg.DomPoint p, double delta) {
    return true;
  }

  public DomLength stroke_width_to_lenght ()
  {
    var s = new AnimatedLength ();
    if (stroke_width == null) {
      if (this is DomStylable) {
        var v = ((DomStylable) this).get_presentation_attribute ("stroke-width");
        if (v != null) {
          s.value = v.css_text;
        }
      }
    } else {
      s.value = stroke_width;
    }
    return s.base_val;
  }

  public virtual DomPoint transform_point (DomPoint p) throws GLib.Error {
    DomPoint res = new Point ();
    res.x = p.x;
    res.y = p.y;
    if (this is DomTransformable) {
      var list = ((DomTransformable) this).transformation_matrix ();
      var trm = Graphene.Matrix ();
      trm.init_identity ();
      for (int i = 0; i < list.get_n_items (); i++) {
        trm = trm.multiply (((DomMatrix) list.get_item (i)).matrix);
      }
      var mat = new Matrix ();
      mat.matrix.init_from_matrix (trm);
      DomMatrix.transform_point (mat, p, out res);
    }
    return res;
  }
}
