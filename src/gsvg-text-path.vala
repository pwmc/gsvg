/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* css-classes.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GLib;
using Gee;

public class GSvg.TextPathElement : TextContentElement,
                                   DomURIReference,
                                   DomTextPathElement {
  public DomAnimatedLength start_off_set { get; set; }
  [Description (nick="::startOffset")]
  public AnimatedLength mstart_off_set {
    get { return start_off_set as AnimatedLength; }
    set { start_off_set = value as DomAnimatedLength; }
  }
  public DomAnimatedEnumeration method { get; set; }
  [Description (nick="::method")]
  public AnimatedEnumeration mmethod {
    get { return method as AnimatedEnumeration; }
    set { method = value as DomAnimatedEnumeration; }
  }
  public DomAnimatedEnumeration spacing { get; set; }
  [Description (nick="::spacing")]
  public AnimatedEnumeration mspacing {
    get { return spacing as AnimatedEnumeration; }
    set { spacing = value as DomAnimatedEnumeration; }
  }
  // URIReference
  public DomAnimatedString href { get; set; }
  [Description (nick="::xlink:href")]
  public AnimatedString mhref {
    get { return href as AnimatedString; }
    set { href = value as DomAnimatedString; }
  }
}

