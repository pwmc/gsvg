/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-gs-animate-color-element-map.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

public class GSvg.AnimateColorElementMap : GXml.HashMap, GSvg.DomAnimateColorElementMap {
  public int length { get { return ((GXml.HashMap) this).length; } }
  construct { try { initialize (typeof (AnimateColorElement)); } catch (GLib.Error e) { warning ("Error: "+e.message); } }
  public new DomAnimateColorElement DomAnimateColorElementMap.get (string id) {
    return ((GXml.HashMap) this).get (id) as DomAnimateColorElement;
  }
  public new void append (DomAnimateColorElement el) {
    try {
      ((GXml.HashMap) this).append (el);
    } catch (GLib.Error e) {
      warning ("Error adding object to clip paths collection: %s", e.message);
    }
  }
}
