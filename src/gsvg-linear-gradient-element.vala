/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-gs-linear-gradient-element.vala
 *
 * Copyright (C) 2016, 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

public class GSvg.LinearGradientElement : GradientElement,
                           GSvg.DomLinearGradientElement, GXml.MappeableElement
{
  private AnimatedLength _mx1;
  public DomAnimatedLength x1 { get { return mx1 as DomAnimatedLength; } }
  [Description (nick="::x1")]
  public AnimatedLength mx1 {
    get { return _mx1; }
    set { _mx1 = value; }
  }
  private AnimatedLength _my1;
  public DomAnimatedLength y1 { get { return _my1 as DomAnimatedLength; }  }
  [Description (nick="::y1")]
  public AnimatedLength my1 {
    get { return _my1; }
    set { _my1 = value; }
  }
  private AnimatedLength _mx2;
  public DomAnimatedLength x2 { get { return _mx2 as DomAnimatedLength; } }
  [Description (nick="::x2")]
  public AnimatedLength mx2 {
    get { return _mx2; }
    set { _mx2 = value; }
  }
  private AnimatedLength _my2;
  public DomAnimatedLength y2 { get { return _my2 as DomAnimatedLength; } }
  [Description (nick="::y2")]
  public AnimatedLength my2 {
    get { return _my2; }
    set { _my2 = value; }
  }
  construct {
    initialize ("linearGradient");
  }
  // MappeableElement
  public string get_map_key () { return id; }
}
