/* gsvg-gs-gradient-element.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

public class GSvg.GradientElement : Stylable,
                               DomURIReference,
                               DomExternalResourcesRequired,
                               DomGradientElement
{

  // URIReference
  public DomAnimatedString href { get; set; }
  [Description (nick="::href")]
  public AnimatedString mhref {
    get { return href as AnimatedString; }
    set { href = value as DomAnimatedString; }
  }

  // ExternalResourcesRequired
  // externalResourcesRequired
  DomAnimatedBoolean _external_resources_required;
  public DomAnimatedBoolean external_resources_required {
    get { return _external_resources_required; }
  }
  // GradientElement
  public  DomAnimatedEnumeration gradient_units {
    get { return mgradient_units as DomAnimatedEnumeration; }
  }
  [Description (nick="::gradientUnits")]
  public AnimatedEnumeration mgradient_units {
    get { return mgradient_units; }
    set { mgradient_units = value; }
  }
  public  DomAnimatedTransformList gradient_transform {
    get { return mgradient_transform as DomAnimatedTransformList; }
  }
  [Description (nick="::gradientTransform")]
  public AnimatedTransformList mgradient_transform {
    get { return mgradient_transform; }
    set { mgradient_transform = value; }
  }
  public  DomAnimatedEnumeration spread_method {
    get { return mspread_method as DomAnimatedEnumeration; }
  }
  [Description (nick="::spreadMethod")]
  public AnimatedEnumeration mspread_method {
    get { return mgradient_units; }
    set { mgradient_units = value; }
  }
}
