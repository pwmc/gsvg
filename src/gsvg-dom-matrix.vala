/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-interfaces-coord.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

public interface GSvg.DomMatrix : GLib.Object {
  public abstract Graphene.Matrix matrix { get; }
  public abstract double          a { get; set; }
  public abstract double          b { get; set; }
  public abstract double          c { get; set; }
  public abstract double          d { get; set; }
  public abstract double          e { get; set; }
  public abstract double          f { get; set; }

  public abstract DomMatrix multiply (DomMatrix second_matrix);
  public abstract DomMatrix inverse () throws GLib.Error;
  public abstract DomMatrix translate (double x, double y);
  public abstract DomMatrix scale (double scale_factor);
  public abstract DomMatrix scale_non_uniform (double scale_factor_x, double scale_factor_y);
  public abstract DomMatrix rotate (double angle);
  public abstract DomMatrix rotate_from_vector (double x, double y) throws GLib.Error;
  public abstract DomMatrix flip_x ();
  public abstract DomMatrix flip_y ();
  public abstract DomMatrix skew_x (double angle);
  public abstract DomMatrix skew_y (double angle);

  public abstract void init (double va, double vb, double vc, double vd, double ve, double vf);

  public static void transform_point (DomMatrix matrix, DomPoint p, out DomPoint np) {
    np = new Point ();
    np.x = p.x;
    np.y = p.y;
    float[] vals = {1,0,0,0,
                      0,1,0,0,
                      0,0,1,0,
                      0,0,0,1};
    vals[0] = (float) p.x;
    vals[4] = (float) p.y;
    var m = Graphene.Matrix ();
    m.init_from_float (vals);
    var r = m.multiply (matrix.matrix);
    np.x = r.get_value (0,0);
    np.y = r.get_value (0,1);
  }
}

public errordomain GSvg.DomMatrixError {
  INVALID_TRANSFORMATION_ERROR
}
