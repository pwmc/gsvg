/* gsvg-dom-interfaces-text.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * aint with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

namespace GSvg {

public interface DomTextContentElement : GLib.Object,
                                  DomElement,
                                  DomTests,
                                  DomLangSpace,
                                  DomExternalResourcesRequired,
                                  DomStylable
{
  public abstract DomAnimatedLength text_length { get; }
  public abstract DomAnimatedEnumeration length_adjust { get; }

  public abstract int get_number_of_chars ();
  public abstract double get_computed_text_length();
  public abstract double get_sub_string_length(int charnum, int nchars) throws GLib.Error;
  public abstract DomPoint get_start_position_of_char(int charnum) throws GLib.Error;
  public abstract DomPoint get_end_position_of_char(int charnum) throws GLib.Error;
  public abstract DomRect get_extent_of_char(int charnum) throws GLib.Error;
  public abstract double get_rotation_of_char(int charnum) throws GLib.Error;
  public abstract int get_char_num_at_position(DomPoint point);
  public abstract void select_sub_string(int charnum, int nchars) throws GLib.Error;
}

  // lengthAdjust Types
public enum LengthAdjust {
  UNKNOWN = 0,
  SPACING = 1,
  SPACINGANDGLYPHS = 2
}

public interface DomTextPositioningElement : GLib.Object,
                                   DomTextContentElement {
  public abstract DomAnimatedLengthList x { get; set; }
  public abstract DomAnimatedLengthList y { get; set; }
  public abstract DomAnimatedLengthList dx { get; set; }
  public abstract DomAnimatedLengthList dy { get; set; }
  public abstract DomAnimatedNumberList rotate { get; set; }
}

public interface DomTextElement : GLib.Object,
                                   DomTextPositioningElement,
                                   DomTransformable
{
  // API Additions
  public abstract DomTSpanElementMap spans { get; }
  public abstract DomTRefElementMap trefs { get; }
  /**
   * Adds a new {@link GXml.DomText} node with the given text
   */
  public abstract GXml.DomText         add_text (string txt);
  /**
   * Creates a new detached {@link GXml.DomText}
   */
  [Version (since="0.6")]
  public abstract GXml.DomText         create_text (string txt);
  /**
   * Add a new {@link DomTSpanElement} at the end of child nodes,
   * with a child {@link GXml.DomText} containing given text
   */
  public abstract DomTSpanElement    add_span (string txt);
  /**
   * Creates a new detached {@link DomTSpanElement},
   * with a child {@link GXml.DomText} containing given text
   */
  [Version (since="0.6")]
  public abstract DomTSpanElement create_span (string txt);
  /**
   * Creates a new empty detached {@link DomTSpanElement}
   */
  [Version (since="0.6")]
  public abstract DomTSpanElement create_empty_span ();
  /**
   * Add a new {@link TRefElement} at the end of child nodes,
   * referencing a text defined at {@link DefsElement}
   */
  public abstract DomTRefElement     add_ref (string id_ref);
  /**
   * Creates a new {@link TRefElement},
   * referencing a text defined at {@link DefsElement}
   */
  [Version (since="0.6")]
  public abstract DomTRefElement create_ref (string id_ref);
  /**
   * Add a new {@link TextPathElement} at the end of child nodes,
   * referencing a path in defined at {@link DefsElement} and using
   * given offset
   */
  [Version (since="0.6")]
  public abstract DomTextPathElement add_path (string path_ref, double start_off_set);
  /**
   * Creates a new detached {@link TextPathElement},
   * referencing a path in defined at {@link DefsElement} and using
   * given offset.
   *
   * {@link TextPathElement.method} and {@link TextPathElement.spacing}
   * are set to {@link TextPathMethodType.UNKNOWN} and {@link TextPathSpacingType.UNKNOWN}
   * respectivelly
   */
  [Version (since="0.6")]
  public abstract DomTextPathElement create_path (string path_ref, double start_off_set);
  /**
   * Creates a new empty detached {@link TextPathElement}
   */
  [Version (since="0.6")]
  public abstract DomTextPathElement create_empty_path ();
}

public interface DomTextElementMap : GLib.Object {
  public abstract int length { get; }
  public abstract DomTextElement @get (string id);
  public abstract void append (DomTextElement el);
}
public interface DomTSpanElement : GLib.Object,
                                   DomTextPositioningElement {
}
public interface DomTSpanElementMap : GLib.Object {
  public abstract int length { get; }
  public abstract DomTSpanElement @get (string id);
  public abstract void append (DomTSpanElement el);
}

public interface DomTRefElement : GLib.Object,
                                   DomTextPositioningElement,
                                   DomURIReference {
}
public interface DomTRefElementMap : GLib.Object {
  public abstract int length { get; }
  public abstract DomTRefElement get (string id);
  public abstract void append (DomTRefElement el);
}

public interface DomTextPathElement : GLib.Object,
                                   DomTextContentElement,
                                   DomURIReference {
  public abstract DomAnimatedLength start_off_set { get; set; }
  public abstract DomAnimatedEnumeration method { get; set; }
  public abstract DomAnimatedEnumeration spacing { get; set; }
}


// textPath Method Types
public enum TextPathMethodType {
  UNKNOWN = 0,
  ALIGN = 1,
  STRETCH = 2
}

  // textPath Spacing Types
public enum TextPathSpacingType {
  UNKNOWN = 0,
  AUTO = 1,
  EXACT = 2
}

public interface DomAltGlyphElement : GLib.Object,
                                   DomTextPositioningElement,
                                   DomURIReference {
  public abstract string glyph_ref { get; set; }
  public abstract string format { get; set; }
}

public interface DomAltGlyphDefElement : GLib.Object, DomElement {
}

public interface DomAltGlyphItemElement : GLib.Object, DomElement {
}

public interface DomGlyphRefElement : GLib.Object,
                               DomElement,
                               DomURIReference,
                               DomStylable {
  public abstract string glyph_ref { get; set; }
  public abstract string format { get; set; }
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double dx { get; set; }
  public abstract double dy { get; set; }
}

} // GSvg
