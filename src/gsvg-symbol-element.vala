/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-gs-symbol-element.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using Gee;


public class GSvg.SymbolElement : GSvg.Stylable,
                                    DomLangSpace,
                                    DomExternalResourcesRequired,
                                    DomFitToViewBox,
                                    DomSymbolElement
{
  protected DomAnimatedBoolean _external_resources_required;
  // LangSpace
  [Description (nick="::xml:lang")]
  public string xmllang { get; set; }
  [Description (nick="::xml:space")]
  public string xmlspace { get; set; }

  // ExternalResourcesRequired
  public DomAnimatedBoolean external_resources_required {
    get { return _external_resources_required; }
  }

  // FitToViewBox
  // * viewBox
  public DomAnimatedRect view_box { get; set; }
  [Description (nick="::viewBox")]
  public AnimatedRect mview_box {
    get { return view_box as AnimatedRect; }
    set { view_box = value as DomAnimatedRect; }
  }
  // * preserveAspectRatio
  public DomAnimatedPreserveAspectRatio preserve_aspect_ratio { get; set; }
  [Description (nick="::preserveAspectRatio")]
  public AnimatedPreserveAspectRatio mpreserve_aspect_ratio {
    get { return preserve_aspect_ratio as AnimatedPreserveAspectRatio; }
    set { preserve_aspect_ratio = value as DomAnimatedPreserveAspectRatio; }
  }

  construct {
    _local_name = "symbol";
  }
}
