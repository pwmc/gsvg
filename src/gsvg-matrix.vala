/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-matrix.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GLib;
using Gee;

public class GSvg.Matrix : GLib.Object, DomMatrix {
  private Graphene.Matrix _matrix = Graphene.Matrix ();
  public Graphene.Matrix matrix { get { return _matrix;} }
  public double a {
    get {
      return _matrix.get_value (0,0);
    }
    set {
      float[] vals = {1,0,0,0,
                        0,1,0,0,
                        0,0,1,0,
                        0,0,0,1};
      vals[0] = (float) value;
      vals[1] = (float) c;
      vals[2] = (float) e;
      vals[4] = (float) b;
      vals[5] = (float) d;
      vals[6] = (float) f;
      _matrix.init_from_float (vals);
    }
  }
  public double b {
    get {
      return _matrix.get_value (1,0);
    }
    set {
      float[] vals = {1,0,0,0,
                        0,1,0,0,
                        0,0,1,0,
                        0,0,0,1};
      vals[0] = (float) a;
      vals[1] = (float) c;
      vals[2] = (float) e;
      vals[4] = (float) value;
      vals[5] = (float) d;
      vals[6] = (float) f;
      _matrix.init_from_float (vals);
    }
  }
  public double c {
    get {
      return _matrix.get_value (0,1);
    }
    set {
      float[] vals = {1,0,0,0,
                        0,1,0,0,
                        0,0,1,0,
                        0,0,0,1};
      vals[0] = (float) a;
      vals[1] = (float) value;
      vals[2] = (float) e;
      vals[4] = (float) b;
      vals[5] = (float) d;
      vals[6] = (float) f;
      _matrix.init_from_float (vals);
    }
  }
  public double d {
    get {
      return _matrix.get_value (1,1);
    }
    set {
      float[] vals = {1,0,0,0,
                        0,1,0,0,
                        0,0,1,0,
                        0,0,0,1};
      vals[0] = (float) a;
      vals[1] = (float) c;
      vals[2] = (float) e;
      vals[4] = (float) b;
      vals[5] = (float) value;
      vals[6] = (float) f;
      _matrix.init_from_float (vals);
    }
  }
  public double e {
    get {
      return _matrix.get_value (0,2);
    }
    set {
      float[] vals = {1,0,0,0,
                        0,1,0,0,
                        0,0,1,0,
                        0,0,0,1};
      vals[0] = (float) a;
      vals[1] = (float) c;
      vals[2] = (float) value;
      vals[4] = (float) b;
      vals[5] = (float) d;
      vals[6] = (float) f;
      _matrix.init_from_float (vals);
    }
  }
  public double f {
    get {
      return _matrix.get_value (1,2);
    }
    set {
      float[] vals = {1,0,0,0,
                        0,1,0,0,
                        0,0,1,0,
                        0,0,0,1};
      vals[0] = (float) a;
      vals[1] = (float) c;
      vals[2] = (float) e;
      vals[4] = (float) b;
      vals[5] = (float) d;
      vals[6] = (float) value;
      _matrix.init_from_float (vals);
    }
  }

  construct {
    _matrix.init_identity ();
  }

  public void init (double va, double vb, double vc, double vd, double ve, double vf) {
    float[] vals = {1,0,0,0,
                      0,1,0,0,
                      0,0,1,0,
                      0,0,0,1};
    vals[0] = (float) va;
    vals[1] = (float) vc;
    vals[2] = (float) ve;
    vals[4] = (float) vb;
    vals[5] = (float) vd;
    vals[6] = (float) vf;
    _matrix.init_from_float (vals);
  }

  public DomMatrix multiply (DomMatrix second_matrix) {
    Graphene.Matrix mres = _matrix.multiply (second_matrix.matrix);
    DomMatrix res = new Matrix ();
    res.matrix.init_from_matrix (mres);
    return res;
  }
  public DomMatrix inverse () throws GLib.Error {
    Graphene.Matrix? mres = null;
    _matrix.inverse (out mres);
    DomMatrix res = new Matrix ();
    res.matrix.init_from_matrix (mres);
    return res;
  }
  public DomMatrix translate (double x, double y) {
    DomMatrix res = new Matrix ();
    res.matrix.init_from_matrix (_matrix);
    Graphene.Point3D pos = Graphene.Point3D ();
    pos.init ((float) x, (float) y, (float) 0.0);
    res.matrix.translate (pos);
    return res;
  }
  public DomMatrix scale (double scale_factor) {
    DomMatrix res = new Matrix ();
    res.matrix.init_from_matrix (_matrix);
    res.matrix.scale ((float) scale_factor, (float) scale_factor, 1);
    return res;
  }
  public DomMatrix scale_non_uniform (double scale_factor_x, double scale_factor_y) {
    DomMatrix res = new Matrix ();
    res.matrix.init_from_matrix (_matrix);
    res.matrix.scale ((float) scale_factor_x, (float) scale_factor_y, 1.0f);
    return res;
  }
  public DomMatrix rotate (double angle) {
    DomMatrix res = new Matrix ();
    res.matrix.init_from_matrix (_matrix);
    res.matrix.rotate_z ((float) angle);
    return res;
  }
  public DomMatrix rotate_from_vector (double x, double y) throws GLib.Error {
    var angle = 90.0;
    if (x != 0.0) {
      angle = y/x * GLib.Math.PI / 180.0;
    } else {
      if (y < 0) {
        angle *= -1;
      }
    }
    return rotate (angle);
  }
  public DomMatrix flip_x () {
    DomMatrix res = new Matrix ();
    float[] vals = {-1,0,0,0,
                      1,0,0,0,
                      0,0,1,0,
                      0,0,0,1};
    res.matrix.init_from_matrix (_matrix);
    var flipx = Graphene.Matrix ();
    flipx.init_from_float (vals);
    var ml = res.matrix.multiply (flipx);
    res.matrix.init_from_matrix (ml);
    return res;
  }
  public DomMatrix flip_y () {
    DomMatrix res = new Matrix ();
    float[] vals = {1,0,0,0,
                      -1,0,0,0,
                      0,0,1,0,
                      0,0,0,1};
    res.matrix.init_from_matrix (_matrix);
    var flipx = Graphene.Matrix ();
    flipx.init_from_float (vals);
    var ml = res.matrix.multiply (flipx);
    res.matrix.init_from_matrix (ml);
    return res;
  }
  public DomMatrix skew_x (double angle) {
    DomMatrix res = new Matrix ();
    res.matrix.init_from_matrix (_matrix);
    res.matrix.skew_xz ((float) angle);
    return res;
  }
  public DomMatrix skew_y (double angle) {
    DomMatrix res = new Matrix ();
    res.matrix.init_from_matrix (_matrix);
    res.matrix.skew_yz ((float) angle);
    return res;
  }
  public DomMatrix apply_transformations (GLib.ListModel list) throws GLib.Error {
    if (!list.get_item_type ().is_a (typeof (DomMatrix))) {
      throw new DomMatrixError.INVALID_TRANSFORMATION_ERROR ("Invalid object list of transformations");
    }
    Graphene.Matrix mres = Graphene.Matrix ();
    mres.init_from_matrix (_matrix);
    for (int i = 0; i < list.get_n_items (); i++) {
      var item = list.get_item (i) as DomMatrix;
      if (item == null) {
        warning ("Invalid object transformation in the list: skipping");
        continue;
      }
      mres = mres.multiply (item.matrix);
    }
    DomMatrix res = new Matrix ();
    res.matrix.init_from_matrix (mres);
    return res;
  }
}
