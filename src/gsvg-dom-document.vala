/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-document.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GXml;

public interface GSvg.DomDocument : GLib.Object,  GXml.DomDocument {
  public abstract string title { owned get; }
  public abstract string referrer { get; }
  public abstract string domain { get; }
  public abstract string url { get; }
  public abstract DomSvgElement? root_element { owned get; }

  /**
   * Adds an 'svg' to the document. If no top most element in the tree exists,
   * this will be the one; it one exists already, a new {@link DomSvgElement} child
   * will appended to {@link root_element}
   *
   * If you plan to set more complex descriptions, set @param desc to null.
   *
   * @param x a string representation of {@link DomAnimatedLength}, for x position, or null
   * @param y a string representation of {@link DomAnimatedLength}, for y position, or null
   * @param width a string representation of {@link DomAnimatedLength}, for width, or null
   * @param height a string representation of {@link DomAnimatedLength}, for height, or null
   * @param viewbox a string representation of {@link DomAnimatedRect}, or null
   * @param title a string for SVG title, or null
   * @param desc a string for a text description, or null
   */
  public abstract DomSvgElement add_svg (string? x,
                            string? y,
                            string? width,
                            string? height,
                            string? viewbox = null,
                            string? title = null,
                            string? desc = null);
  /**
   * Creates a detached 'svg' element. It then can be appended as a child
   * of container elements.
   *
   * If you plan to set more complex descriptions, set @param desc to null.
   *
   * @param x a string representation of {@link AnimatedLength}, for x position, or null
   * @param y a string representation of {@link AnimatedLength}, for y position, or null
   * @param width a string representation of {@link AnimatedLength}, for width, or null
   * @param height a string representation of {@link AnimatedLength}, for height, or null
   * @param viewbox a string representation of {@link AnimatedRect}, or null
   * @param title a string for SVG title, or null
   * @param desc a string for a text description, or null
   */
  public abstract DomSvgElement create_svg (string? x,
                            string? y,
                            string? width,
                            string? height,
                            string? viewbox = null,
                            string? title = null,
                            string? desc = null);
}

