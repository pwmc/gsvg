/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-gs-angle.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GLib;
using Gee;

public class GSvg.Angle : GLib.Object, GSvg.DomAngle {
  public DomAngle.Type unit_type { get; set; }
  public double value { get; set; }
  public double value_in_specified_units { get; set; }
  public string value_as_string { get; set; }

  public void new_value_specified_units (DomAngle.Type unit_type, double value_in_specified_units) throws GLib.Error {}
  public void convert_to_specified_units (DomAngle.Type unit_type) throws GLib.Error {}
}
