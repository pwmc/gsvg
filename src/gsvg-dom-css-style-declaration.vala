/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* css-interfaces.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using GXml;
// FIXME: This should be moved to its own library
/**
 * CSS interfaces according with [[https://www.w3.org/TR/SVG/]] version 1.1
 */
public interface GSvg.DomCSSStyleDeclaration : GLib.Object, GXml.Property {
  public abstract string        css_text { get; set; }
  public abstract uint          length { get; set; }
  public abstract CSSRule       parent_rule { get; set; }

  public abstract string        get_property_value (string property_name);
  public abstract CSSValue      get_property_css_value (string property_name);
  public abstract string        remove_property (string property_name) throws GLib.Error;
  public abstract string        get_property_priority (string property_name);
  public new abstract void      set_property (string property_name,
                                             string value,
                                             string priority) throws GLib.Error;
  public abstract string        item (int index);
  public abstract void inherit (string style);
}

