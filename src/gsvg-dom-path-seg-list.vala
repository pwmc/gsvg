/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-path-seg-list.vala
 *
 * Copyright (C) 2016,2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GSvg.DomPathSegList : GLib.Object {

  public abstract int number_of_items { get;}

  public abstract void clear() throws GLib.Error;
  public abstract DomPathSeg initialize (DomPathSeg new_item) throws GLib.Error;
  public abstract DomPathSeg get_item (int index) throws GLib.Error;
  public abstract DomPathSeg insert_item_before (DomPathSeg new_item, int index) throws GLib.Error;
  public abstract DomPathSeg replace_item (DomPathSeg new_item, int index) throws GLib.Error;
  public abstract DomPathSeg remove_item (int index) throws GLib.Error;
  public abstract DomPathSeg append_item (DomPathSeg new_item) throws GLib.Error;

  public abstract void parse (string str) throws GLib.Error;

  public static string parse_command (string str, out string unparsed) {
    unparsed = str;
    if (str.length == 0) {
      return "";
    }
    int i = 0;
    unichar c = str.get_char (i);
    while (i < str.length) {
      if (c.isspace ()) {
        i++;
        c = str.get_char (i);
        continue;
      }
      if (!c.isalpha ()) {
        unparsed = str.substring (i, -1);
        return "";
      }
      if (c.isalpha ()) {
        if (i + 1 < str.length) {
          unparsed = str.substring (i + 1, -1);
        } else {
          unparsed = null;
        }
        return c.to_string ();
      }
    }
    return "";
  }
  public string to_string () {
    string str = "";
    for (int i = 0; i < number_of_items; i++) {
      try {
        var seg = get_item (i);
        if (seg is DomPathSegMovetoAbs) {
          str += "M";
          str += DomPoint.double_to_string (((DomPathSegMovetoAbs) seg).x) + ",";
          str += DomPoint.double_to_string (((DomPathSegMovetoAbs) seg).y);
        }
        if (seg is DomPathSegMovetoRel) {
          str += "m";
          str += DomPoint.double_to_string (((DomPathSegMovetoRel) seg).x) + ",";
          str += DomPoint.double_to_string (((DomPathSegMovetoRel) seg).y);
        }
        if (seg is DomPathSegClosePath) {
          str += "z";
        }
        if (seg is DomPathSegLinetoAbs) {
          str += "L";
          str += DomPoint.double_to_string (((DomPathSegLinetoAbs) seg).x) + ",";
          str += DomPoint.double_to_string (((DomPathSegLinetoAbs) seg).y);
        }
        if (seg is DomPathSegLinetoRel) {
          str += "l";
          str += DomPoint.double_to_string (((DomPathSegLinetoRel) seg).x) + ",";
          str += DomPoint.double_to_string (((DomPathSegLinetoRel) seg).y);
        }
        if (seg is DomPathSegLinetoHorizontalAbs) {
          str += "H";
          str += DomPoint.double_to_string (((DomPathSegLinetoHorizontalAbs) seg).x);
        }
        if (seg is DomPathSegLinetoHorizontalRel) {
          str += "h";
          str += DomPoint.double_to_string (((DomPathSegLinetoHorizontalRel) seg).x);
        }
        if (seg is DomPathSegLinetoVerticalAbs) {
          str += "V";
          str += DomPoint.double_to_string (((DomPathSegLinetoVerticalAbs) seg).y);
        }
        if (seg is DomPathSegLinetoVerticalRel) {
          str += "v";
          str += DomPoint.double_to_string (((DomPathSegLinetoVerticalRel) seg).y);
        }
        if (seg is DomPathSegCurvetoCubicAbs) {
          str += "C";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicAbs) seg).x1) + ",";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicAbs) seg).y1) + " ";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicAbs) seg).x2) + ",";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicAbs) seg).y2) + " ";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicAbs) seg).x) + ",";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicAbs) seg).y);
        }
        if (seg is DomPathSegCurvetoCubicRel) {
          str += "c";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicRel) seg).x1) + ",";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicRel) seg).y1) + " ";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicRel) seg).x2) + ",";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicRel) seg).y2) + " ";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicRel) seg).x) + ",";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicRel) seg).y);
        }
        if (seg is DomPathSegCurvetoCubicSmoothAbs) {
          str += "S";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicSmoothAbs) seg).x2) + ",";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicSmoothAbs) seg).y2) + " ";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicSmoothAbs) seg).x) + ",";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicSmoothAbs) seg).y);
        }
        if (seg is DomPathSegCurvetoCubicSmoothRel) {
          str += "s";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicSmoothRel) seg).x2) + ",";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicSmoothRel) seg).y2) + " ";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicSmoothRel) seg).x) + ",";
          str += DomPoint.double_to_string (((DomPathSegCurvetoCubicSmoothRel) seg).y);
        }
        if (seg is DomPathSegCurvetoQuadraticAbs) {
          str += "Q";
          str += DomPoint.double_to_string (((DomPathSegCurvetoQuadraticAbs) seg).x1) + ",";
          str += DomPoint.double_to_string (((DomPathSegCurvetoQuadraticAbs) seg).y1) + " ";
          str += DomPoint.double_to_string (((DomPathSegCurvetoQuadraticAbs) seg).x) + ",";
          str += DomPoint.double_to_string (((DomPathSegCurvetoQuadraticAbs) seg).y);
        }
        if (seg is DomPathSegCurvetoQuadraticRel) {
          str += "q";
          str += DomPoint.double_to_string (((DomPathSegCurvetoQuadraticRel) seg).x1) + ",";
          str += DomPoint.double_to_string (((DomPathSegCurvetoQuadraticRel) seg).y1) + " ";
          str += DomPoint.double_to_string (((DomPathSegCurvetoQuadraticRel) seg).x) + ",";
          str += DomPoint.double_to_string (((DomPathSegCurvetoQuadraticRel) seg).y);
        }
        if (seg is DomPathSegCurvetoQuadraticSmoothAbs) {
          str += "T";
          str += DomPoint.double_to_string (((DomPathSegCurvetoQuadraticSmoothAbs) seg).x) + ",";
          str += DomPoint.double_to_string (((DomPathSegCurvetoQuadraticSmoothAbs) seg).y);
        }
        if (seg is DomPathSegCurvetoQuadraticSmoothRel) {
          str += "t";
          str += DomPoint.double_to_string (((DomPathSegCurvetoQuadraticSmoothRel) seg).x) + ",";
          str += DomPoint.double_to_string (((DomPathSegCurvetoQuadraticSmoothRel) seg).y);
        }
        if (seg is DomPathSegArcAbs) {
          str += "A";
          str += DomPoint.double_to_string (((DomPathSegArcAbs) seg).r1) + ",";
          str += DomPoint.double_to_string (((DomPathSegArcAbs) seg).r2) + " ";
          str += DomPoint.double_to_string (((DomPathSegArcAbs) seg).angle) + " ";
          str += ((DomPathSegArcAbs) seg).large_arc_flag ? "1" : "0";
          str += ",";
          str += ((DomPathSegArcAbs) seg).sweep_flag ? "1" : "0";
          str += " ";
          str += DomPoint.double_to_string (((DomPathSegArcAbs) seg).x) + ",";
          str += DomPoint.double_to_string (((DomPathSegArcAbs) seg).y);
        }
        if (seg is DomPathSegArcRel) {
          str += "a";
          str += DomPoint.double_to_string (((DomPathSegArcRel) seg).r1) + ",";
          str += DomPoint.double_to_string (((DomPathSegArcRel) seg).r2) + " ";
          str += DomPoint.double_to_string (((DomPathSegArcRel) seg).angle) + " ";
          str += ((DomPathSegArcRel) seg).large_arc_flag ? "1" : "0";
          str += ",";
          str += ((DomPathSegArcRel) seg).sweep_flag ? "1" : "0";
          str += " ";
          str += DomPoint.double_to_string (((DomPathSegArcRel) seg).x) + ",";
          str += DomPoint.double_to_string (((DomPathSegArcRel) seg).y);
        }
        if (i + 1 < number_of_items) {
          str += " ";
        }
      } catch (GLib.Error e) {
        warning ("Error getting item: %s", e.message);
        continue;
      }
    }
    return str;
  }
}

public errordomain GSvg.DomPathSegListError {
  INVALID_NOTATION_ERROR
}

