/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* css-classes.vala
 *
 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class GSvg.AnimatedTransformList : GLib.Object,
                                          GXml.Property,
                                          DomAnimatedTransformList
{
  public DomTransformList base_val { get; set; }
  public DomTransformList anim_val { get; set; }
  public string? value {
    owned get {
      if (base_val == null) return null;
      return base_val.value;
    }
    set {
      if (base_val == null)
        base_val = new TransformList ();
      base_val.value = value;
    }
  }
  public bool validate_value (string? val) {
    if (base_val == null) base_val = new TransformList ();
    return base_val.validate_value (val);
  }
}
