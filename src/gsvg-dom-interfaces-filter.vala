/* gsvg-dom-public interfaces-filter.vala
 *
 * Copyright (C) 2016,2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

namespace GSvg {

public interface DomFilterElement : GLib.Object, DomElement,
                             DomURIReference,
                             DomLangSpace,
                             DomExternalResourcesRequired,
                             DomStylable {

  public abstract DomAnimatedEnumeration filter_units { get; }
  public abstract DomAnimatedEnumeration primitive_units { get; }
  public abstract DomAnimatedLength x { get; }
  public abstract DomAnimatedLength y { get; }
  public abstract DomAnimatedLength width { get; }
  public abstract DomAnimatedLength height { get; }
  public abstract DomAnimatedInteger filter_res_x { get; }
  public abstract DomAnimatedInteger filter_res_y { get; }

  public abstract void setFilterRes(uint filterResX, uint filterResY) throws GLib.Error;
}

public interface DomFilterPrimitiveStandardAttributes : GLib.Object, DomStylable {
  public abstract DomAnimatedLength x { get; }
  public abstract DomAnimatedLength y { get; }
  public abstract DomAnimatedLength width { get; }
  public abstract DomAnimatedLength height { get; }
  public abstract DomAnimatedString result { get; }
}

public interface DomFEBlendElement : GLib.Object, DomElement,
                              DomFilterPrimitiveStandardAttributes {
  public abstract DomAnimatedString in1 { get; }
  public abstract DomAnimatedString in2 { get; }
  public abstract DomAnimatedEnumeration mode { get; }
}

  // Blend Mode Types
public enum FEBlendMode {
  UNKNOWN = 0,
  NORMAL = 1,
  MULTIPLY = 2,
  SCREEN = 3,
  DARKEN = 4,
  LIGHTEN = 5
}

public interface DomFEColorMatrixElement : GLib.Object, DomElement,
                                    DomFilterPrimitiveStandardAttributes {
  public abstract DomAnimatedString in1 { get; }
  public abstract DomAnimatedEnumeration cm_type { get; }
  public abstract DomAnimatedNumberList values { get; }
}

  // Color Matrix Types
public enum FEColorMatrixType {
  UNKNOWN = 0,
  MATRIX = 1,
  SATURATE = 2,
  HUEROTATE = 3,
  LUMINANCETOALPHA = 4
}


public interface DomFEComponentTransferElement : GLib.Object, DomElement,
                                          DomFilterPrimitiveStandardAttributes {
  public abstract DomAnimatedString in1 { get; }
}

public interface DomComponentTransferFunctionElement : GLib.Object, DomElement {
  public abstract DomAnimatedEnumeration ctf_type { get; }
  public abstract DomAnimatedNumberList table_values { get; }
  public abstract DomAnimatedNumber slope { get; }
  public abstract DomAnimatedNumber intercept { get; }
  public abstract DomAnimatedNumber amplitude { get; }
  public abstract DomAnimatedNumber exponent { get; }
  public abstract DomAnimatedNumber offset { get; }
}


  // Component Transfer Types
public enum FEComponentTransferType {
  UNKNOWN = 0,
  IDENTITY = 1,
  TABLE = 2,
  DISCRETE = 3,
  LINEAR = 4,
  GAMMA = 5
}


public interface DomFEFuncRElement : GLib.Object, DomComponentTransferFunctionElement {
}

public interface DomFEFuncGElement : GLib.Object, DomComponentTransferFunctionElement {
}

public interface DomFEFuncBElement : GLib.Object, DomComponentTransferFunctionElement {
}

public interface DomFEFuncAElement : GLib.Object, DomComponentTransferFunctionElement {
}

public interface DomFECompositeElement : GLib.Object,
                                  DomElement,
                                  DomFilterPrimitiveStandardAttributes {
  public abstract DomAnimatedString in1 { get; }
  public abstract DomAnimatedString in2 { get; }
  public abstract DomAnimatedEnumeration operator { get; }
  public abstract DomAnimatedNumber k1 { get; }
  public abstract DomAnimatedNumber k2 { get; }
  public abstract DomAnimatedNumber k3 { get; }
  public abstract DomAnimatedNumber k4 { get; }
}


  // Composite Operators
public enum FE0CompositeOperator {
  UNKNOWN = 0,
  OVER = 1,
  IN = 2,
  OUT = 3,
  ATOP = 4,
  XOR = 5,
  ARITHMETIC = 6,
}

public interface DomFEConvolveMatrixElement : GLib.Object,
                                      DomElement,
                                      DomFilterPrimitiveStandardAttributes {

  public abstract DomAnimatedString in1 { get; }
  public abstract DomAnimatedInteger order_x { get; }
  public abstract DomAnimatedInteger order_y { get; }
  public abstract DomAnimatedNumberList kernel_matrix { get; }
  public abstract DomAnimatedNumber divisor { get; }
  public abstract DomAnimatedNumber bias { get; }
  public abstract DomAnimatedInteger target_x { get; }
  public abstract DomAnimatedInteger target_y { get; }
  public abstract DomAnimatedEnumeration edge_mode { get; }
  public abstract DomAnimatedNumber kernel_unit_length_x { get; }
  public abstract DomAnimatedNumber kernel_unit_length_y { get; }
  public abstract DomAnimatedBoolean preserve_alpha { get; }
}

  // Edge Mode Values
public enum EdgeMode {
  UNKNOWN = 0,
  DUPLICATE = 1,
  WRAP = 2,
  NONE = 3
}

public interface DomFEDiffuseLightingElement : GLib.Object,
                                        DomElement,
                                        DomFilterPrimitiveStandardAttributes {
  public abstract DomAnimatedString in1 { get; }
  public abstract DomAnimatedNumber surface_scale { get; }
  public abstract DomAnimatedNumber diffuse_constant { get; }
  public abstract DomAnimatedNumber kernel_unit_length_x { get; }
  public abstract DomAnimatedNumber kernel_unit_length_y { get; }
}

public interface DomFEDistantLightElement : GLib.Object, DomElement {
  public abstract DomAnimatedNumber azimuth { get; }
  public abstract DomAnimatedNumber elevation { get; }
}

public interface DomFEPointLightElement : GLib.Object, DomElement {
  public abstract DomAnimatedNumber x { get; }
  public abstract DomAnimatedNumber y { get; }
  public abstract DomAnimatedNumber z { get; }
}

public interface DomFESpotLightElement : GLib.Object, DomElement {
  public abstract DomAnimatedNumber x { get; }
  public abstract DomAnimatedNumber y { get; }
  public abstract DomAnimatedNumber z { get; }
  public abstract DomAnimatedNumber points_at_x { get; }
  public abstract DomAnimatedNumber points_at_y { get; }
  public abstract DomAnimatedNumber points_at_z { get; }
  public abstract DomAnimatedNumber specular_exponent { get; }
  public abstract DomAnimatedNumber limiting_cone_angle { get; }
}

public interface DomFEDisplacementMapElement : GLib.Object, DomElement,
                                        DomFilterPrimitiveStandardAttributes {
  public abstract DomAnimatedString in1 { get; }
  public abstract DomAnimatedString in2 { get; }
  public abstract DomAnimatedNumber scale { get; }
  public abstract DomAnimatedEnumeration x_channel_selector { get; }
  public abstract DomAnimatedEnumeration y_channel_selector { get; }
}

  // Channel Selectors
public enum Channel {
  UNKNOWN = 0,
  R = 1,
  G = 2,
  B = 3,
  A = 4
}


public interface DomFEFloodElement : GLib.Object, DomElement,
                              DomFilterPrimitiveStandardAttributes {
}

public interface DomFEGaussianBlurElement : GLib.Object, DomElement,
                                     DomFilterPrimitiveStandardAttributes {

  public abstract DomAnimatedString in1 { get; }
  public abstract DomAnimatedNumber std_deviation_x { get; }
  public abstract DomAnimatedNumber std_deviation_y { get; }

  public abstract void set_std_deviation (double stdDeviationX, double stdDeviationY) throws GLib.Error;
}

public interface DomFEImageElement : GLib.Object, DomElement,
                              DomURIReference,
                              DomLangSpace,
                              DomExternalResourcesRequired,
                              DomFilterPrimitiveStandardAttributes {
  public abstract DomAnimatedPreserveAspectRatio preserve_aspect_ratio { get; }
}

public interface DomFEMergeElement : GLib.Object, DomElement,
                              DomFilterPrimitiveStandardAttributes {
}

public interface DomFEMergeNodeElement : GLib.Object, DomElement {
  public abstract DomAnimatedString in1 { get; }
}

public interface DomFEMorphologyElement : GLib.Object, DomElement,
                                   DomFilterPrimitiveStandardAttributes {
  public abstract DomAnimatedString in1 { get; }
  public abstract DomAnimatedEnumeration operator { get; }
  public abstract DomAnimatedNumber radius_x { get; }
  public abstract DomAnimatedNumber radius_y { get; }
}


  // Morphology Operators
public enum MorphologyOperator {
  UNKNOWN = 0,
  ERODE = 1,
  DILATE = 2
}

public interface DomFEOffsetElement : GLib.Object, DomElement,
                               DomFilterPrimitiveStandardAttributes {
  public abstract DomAnimatedString in1 { get; }
  public abstract DomAnimatedNumber dx { get; }
  public abstract DomAnimatedNumber dy { get; }
}

public interface DomFESpecularLightingElement : GLib.Object, DomElement,
                                         DomFilterPrimitiveStandardAttributes {
  public abstract DomAnimatedString in1 { get; }
  public abstract DomAnimatedNumber surface_scale { get; }
  public abstract DomAnimatedNumber specular_constant { get; }
  public abstract DomAnimatedNumber specular_exponent { get; }
  public abstract DomAnimatedNumber kernel_unit_length_x { get; }
  public abstract DomAnimatedNumber kernel_unit_length_y { get; }
}

public interface DomFETileElement : GLib.Object, DomElement,
                             DomFilterPrimitiveStandardAttributes {
  public abstract DomAnimatedString in1 { get; }
}

public interface DomFETurbulenceElement : GLib.Object, DomElement,
                                   DomFilterPrimitiveStandardAttributes {
  public abstract DomAnimatedNumber base_frequency_x { get; }
  public abstract DomAnimatedNumber base_frequency_y { get; }
  public abstract DomAnimatedInteger num_octaves { get; }
  public abstract DomAnimatedNumber seed { get; }
  public abstract DomAnimatedEnumeration stitch_tiles { get; }
  public abstract DomAnimatedEnumeration ttype { get; }
}


  // Turbulence Types
public enum TurbulenceType {
  UNKNOWN = 0,
  FRACTALNOISE = 1,
  TURBULENCE = 2
}
  // Stitch Options
public enum StichType {
  UNKNOWN = 0,
  STITCH = 1,
  NOSTITCH = 2
}

} // GSvg
