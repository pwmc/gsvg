/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-gs-clip-path-element.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using Gee;

public class GSvg.ClipPathElement : GSvg.Transformable, DomClipPathElement {
  private AnimatedEnumeration _mclip_path_units;
  public DomAnimatedEnumeration clip_path_units { get { return _mclip_path_units as DomAnimatedEnumeration; } }
  [Description (nick="::clipPathUnits")]
  public AnimatedEnumeration mclip_path_units {
    get { return _mclip_path_units; }
    set { _mclip_path_units = value; }
  }
  construct {
    _local_name = "clipPath";
  }
}
