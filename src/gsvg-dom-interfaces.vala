/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-interfaces.vala
 *
 * Copyright (C) 2016-2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

namespace GSvg {

public interface DomAnimatedBoolean : GLib.Object {
  public abstract bool base_val { get; set; }
  public abstract bool anim_val { get; }
}

public interface DomAnimatedEnumeration : GLib.Object {
  public abstract int base_val { get; set; }
  public abstract int anim_val { get; set; }
}

public interface DomAnimatedInteger : GLib.Object {
  public abstract long base_val { get; set; }
  public abstract long anim_val { get; }
}

public interface DomNumber : GLib.Object {
  public abstract double value { get; set; }
}

public interface DomAnimatedNumber : GLib.Object, GXml.Property {
  public abstract double base_val { get; set; }
  public abstract double anim_val  { get; set; }
}

public interface DomNumberList : GLib.Object {

  public abstract int number_of_items { get; }

  public abstract void clear() throws GLib.Error;
  public abstract DomNumber initialize (DomNumber newItem) throws GLib.Error;
  public abstract DomNumber get_item (int index) throws GLib.Error;
  public abstract DomNumber insert_item_before (DomNumber newItem, int index) throws GLib.Error;
  public abstract DomNumber replace_item (DomNumber newItem, int index) throws GLib.Error;
  public abstract DomNumber remove_item (int index) throws GLib.Error;
  public abstract DomNumber append_item (DomNumber newItem) throws GLib.Error;
  /**
   * Translate a string to and from a list of {@link Number} items
   */
  public abstract string? value { owned get; set; }
}

public interface DomAnimatedNumberList : GLib.Object, GXml.Property {
  public abstract DomNumberList base_val { get; set; }
  public abstract DomNumberList anim_val { get; set; }
}

public interface DomAnimatedLength : GLib.Object, GXml.Property {
  // baseVal
  public abstract DomLength base_val { get; construct set; }
  // animVal
  public abstract DomLength anim_val { get; }
}

public interface DomAnimatedLengthX : GLib.Object, DomAnimatedLength {}
public interface DomAnimatedLengthY : GLib.Object, DomAnimatedLength {}
public interface DomAnimatedLengthWidth : GLib.Object, DomAnimatedLength {}
public interface DomAnimatedLengthHeight : GLib.Object, DomAnimatedLength {}
public interface DomAnimatedLengthRX : GLib.Object, DomAnimatedLength {}
public interface DomAnimatedLengthRY : GLib.Object, DomAnimatedLength {}
public interface DomAnimatedLengthCX : GLib.Object, DomAnimatedLength {}
public interface DomAnimatedLengthCY : GLib.Object, DomAnimatedLength {}
public interface DomAnimatedLengthR : GLib.Object, DomAnimatedLength {}

public interface DomLengthList : GLib.Object {

  public abstract int number_of_items { get; }

  public abstract void clear() throws GLib.Error;
  public abstract DomLength initialize (DomLength newItem) throws GLib.Error;
  public abstract DomLength get_item (int index) throws GLib.Error;
  public abstract DomLength insert_item_before (DomLength newItem,int index) throws GLib.Error;
  public abstract DomLength replace_item (DomLength newItem, int index) throws GLib.Error;
  public abstract DomLength remove_item (int index) throws GLib.Error;
  public abstract DomLength append_item (DomLength newItem) throws GLib.Error;
  // Added API
  /**
   * From/to string property, parsing tokens
   */
  public abstract string? value { owned get; set; }
}

public interface DomAnimatedLengthList : GLib.Object, GXml.Property {
  public abstract DomLengthList base_val { get; }
  public abstract DomLengthList anim_val { get; }
}

public interface DomAnimatedAngle : GLib.Object {
  public abstract  DomAngle base_val { get; }
  public abstract  DomAngle anim_val { get; }
}

public interface DomColor : GLib.Object, CSSValue {
  public abstract int color_type { get; }
  public abstract CSSRGBColor rgb_color { get; }
  public abstract DomICCColor icc_color { get; }

  public abstract void set_rgb_color(string rgbColor) throws GLib.Error;
  public abstract void set_rgb_color_icc_color (string rgbColor, string iccColor) throws GLib.Error;
  public abstract void set_color (int colorType, string rgbColor, string iccColor) throws GLib.Error;

  /**
   * Color Types
   */
  public enum Type {
    NKNOWN = 0,
    RGBCOLOR = 1,
    RGBCOLOR_ICCCOLOR = 2,
    CURRENTCOLOR = 3
  }
}

public interface DomICCColor : GLib.Object {
  public abstract string color_profile { get; set; }
  public abstract DomNumberList colors { get; }
}

public interface DomRect : GLib.Object, GXml.Property {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double width { get; set; }
  public abstract double height { get; set; }

  public abstract string to_string ();
  public abstract void parse (string str);
}

public interface DomAnimatedRect : GLib.Object, GXml.Property {
  public abstract DomRect base_val { get; }
  public abstract DomRect anim_val { get; }
}

/**
 * Unit Types
 */
public enum UnitTypes {
  UNKNOWN = 0,
  USERSPACEONUSE = 1,
  OBJECTBOUNDINGBOX = 2
}

public interface DomLocatable : GLib.Object {
  // nearestViewportElement
  public abstract DomElement nearest_viewport_element { get; }
  // farthestViewportElement
  public abstract DomElement farthest_viewport_element { get; }

  public abstract DomRect get_bbox();
  public abstract DomMatrix get_ctm ();
  public abstract DomMatrix get_screen_ctm ();
  public abstract DomMatrix get_transform_to_element (DomElement element) throws GLib.Error;
}

public interface DomTests : GLib.Object {
  // requiredFeatures
  public abstract DomStringList required_features { get; }
  // requiredExtensions
  public abstract DomStringList required_extensions { get; }
  // systemLanguage
  public abstract DomStringList system_language { get; }

  public abstract bool has_extension (string extension);
}

public interface DomLangSpace : GLib.Object {
  public abstract string xmllang { get; set; }
  public abstract string xmlspace { get; set; }
}

public interface DomExternalResourcesRequired : GLib.Object {
  // externalResourcesRequired
  public abstract DomAnimatedBoolean external_resources_required { get; }
}

public interface DomFitToViewBox : GLib.Object {
  // viewBox
  public abstract DomAnimatedRect view_box { get; set; }
  // preserveAspectRatio
  public abstract DomAnimatedPreserveAspectRatio preserve_aspect_ratio { get; set; }
}

public interface DomZoomAndPan : GLib.Object {
  public abstract int zoom_and_pan { get; set; }

  /**
   * Zoom and Pan Types
   */
  public enum Type {
    UNKNOWN = 0,
    DISABLE = 1,
    MAGNIFY = 2
  }
}

public interface DomViewSpec : GLib.Object,
                            DomZoomAndPan,
                            DomFitToViewBox {
  public abstract DomTransformList transform { get; }
  public abstract DomElement view_target { get; }
  public abstract string view_box_string { get; }
  public abstract string preserve_aspect_ratio_string { get; }
  public abstract string transform_string { get; }
  public abstract string view_target_string { get; }
}

public interface DomURIReference : GLib.Object {
  public abstract DomAnimatedString href { get; set; }
}

public interface DomSVGCSSRule : GLib.Object, CSSRule {
  public enum Type {
    COLOR_PROFILE_RULE = 7
  }
}
/**
 * Rendering Intent Types
 */
public enum RenderingIntent {
  UNKNOWN = 0,
  INTENT_AUTO = 1,
  PERCEPTUAL = 2,
  COLORIMETRIC = 3,
  SATURATION = 4,
  ABSOLUTE_COLORIMETRIC = 5
}

}// GSvg
