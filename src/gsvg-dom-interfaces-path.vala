/* gsvg-dom-interfaces-path.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

namespace GSvg {

public interface DomPathSeg : GLib.Object {
  public abstract uint path_seg_type { get;}
  public abstract string path_seg_type_as_letter { get;}

  /**
   * Path Segment Types
   */
  public enum Type {
    UNKNOWN = 0,
    CLOSEPATH = 1,
    MOVETO_ABS = 2,
    MOVETO_REL = 3,
    LINETO_ABS = 4,
    LINETO_REL = 5,
    CURVETO_CUBIC_ABS = 6,
    CURVETO_CUBIC_REL = 7,
    CURVETO_QUADRATIC_ABS = 8,
    CURVETO_QUADRATIC_REL = 9,
    ARC_ABS = 10,
    ARC_REL = 11,
    LINETO_HORIZONTAL_ABS = 12,
    LINETO_HORIZONTAL_REL = 13,
    LINETO_VERTICAL_ABS = 14,
    LINETO_VERTICAL_REL = 15,
    CURVETO_CUBIC_SMOOTH_ABS = 16,
    CURVETO_CUBIC_SMOOTH_REL = 17,
    CURVETO_QUADRATIC_SMOOTH_ABS = 18,
    CURVETO_QUADRATIC_SMOOTH_REL = 19
  }
}

public interface DomPathSegClosePath : GLib.Object, DomPathSeg {
}

public interface DomPathSegMovetoAbs : GLib.Object, DomPathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
}

public interface DomPathSegMovetoRel : GLib.Object, DomPathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
}

public interface DomPathSegLinetoAbs : GLib.Object, DomPathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
}

public interface DomPathSegLinetoRel : GLib.Object, DomPathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
}

public interface DomPathSegCurvetoCubicAbs : GLib.Object, DomPathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double x1 { get; set; }
  public abstract double y1 { get; set; }
  public abstract double x2 { get; set; }
  public abstract double y2 { get; set; }
}

public interface DomPathSegCurvetoCubicRel : GLib.Object, DomPathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double x1 { get; set; }
  public abstract double y1 { get; set; }
  public abstract double x2 { get; set; }
  public abstract double y2 { get; set; }
}

public interface DomPathSegCurvetoQuadraticAbs : GLib.Object, DomPathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double x1 { get; set; }
  public abstract double y1 { get; set; }
}

public interface DomPathSegCurvetoQuadraticRel : GLib.Object, DomPathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double x1 { get; set; }
  public abstract double y1 { get; set; }
}

public interface DomPathSegArcAbs : GLib.Object, DomPathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double r1 { get; set; }
  public abstract double r2 { get; set; }
  public abstract double angle { get; set; }
  public abstract bool large_arc_flag { get; set; }
  public abstract bool sweep_flag { get; set; }
}

public interface DomPathSegArcRel : GLib.Object, DomPathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double r1 { get; set; }
  public abstract double r2 { get; set; }
  public abstract double angle { get; set; }
  public abstract bool large_arc_flag { get; set; }
  public abstract bool sweep_flag { get; set; }
}

public interface DomPathSegLinetoHorizontalAbs : GLib.Object, DomPathSeg {
  public abstract double x { get; set; }
}

public interface DomPathSegLinetoHorizontalRel : GLib.Object, DomPathSeg {
  public abstract double x { get; set; }
}

public interface DomPathSegLinetoVerticalAbs : GLib.Object, DomPathSeg {
  public abstract double y { get; set; }
}

public interface DomPathSegLinetoVerticalRel : GLib.Object, DomPathSeg {
  public abstract double y { get; set; }
}

public interface DomPathSegCurvetoCubicSmoothAbs : GLib.Object, DomPathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double x2 { get; set; }
  public abstract double y2 { get; set; }
}

public interface DomPathSegCurvetoCubicSmoothRel : GLib.Object, DomPathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
  public abstract double x2 { get; set; }
  public abstract double y2 { get; set; }
}

public interface DomPathSegCurvetoQuadraticSmoothAbs : GLib.Object, DomPathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
}

public interface DomPathSegCurvetoQuadraticSmoothRel : GLib.Object, DomPathSeg {
  public abstract double x { get; set; }
  public abstract double y { get; set; }
}

public interface DomAnimatedPathData : GLib.Object {
  /**
   * Once you get a reference of the internal object
   * representing a path segment list, you should:
   *
   * # Get a new reference from the PathElement object
   * # Use parse() on 'd' property of PathElement
   *
   * Above will ensure you have updated list of
   * path segments
   */
  public abstract  DomPathSegList path_seg_list { get;}
  public abstract  DomPathSegList normalized_path_seg_list { get;}
  public abstract  DomPathSegList animated_path_seg_list { get;}
  public abstract  DomPathSegList animated_normalized_path_seg_list { get;}
}

} // GSvg
