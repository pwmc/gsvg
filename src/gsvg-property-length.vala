/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016, 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using Gee;

public class GSvg.AnimatedLength : GLib.Object,
                                    GXml.Property,
                                    DomAnimatedLength
{
  protected DomLength _base_val = new Length () as DomLength;
  protected DomLength _anim_val;
  // GXml.Property
  /**
   * Attribute's value in the parent {@link GXml.DomElement}.
   */
  public string? value {
    owned get { return _base_val.value_as_string; }
    set {
      if (value != null)
        _base_val.value_as_string = value;
    }
  }
  public bool validate_value (string? val) {
    return double.try_parse (val); // FIXME:
  }
  // baseVal
  public DomLength base_val {
    get { return _base_val; }
    construct set {
      if (value is Length)
        _base_val.value_as_string = ((Length) value).to_string ();
    }
  }
  // animVal
  public DomLength anim_val {
    get {
      if (_anim_val == null)
         _anim_val = new Length () as DomLength;
      return _anim_val;
    }
  }
}

public class GSvg.Length : GLib.Object, GSvg.DomLength {
  DomLength.Type _unit_type = DomLength.Type.UNKNOWN;
  double _value_in_specified_units = (float) 0.0;
  string _value_as_string = "0";
  public DomLength.Type unit_type {
    get {
      return _unit_type;
    }
    construct set {
      _unit_type = value;
      _value_as_string = this.to_string ();
    }
  }
  public double value {
    get {
      return _value_in_specified_units;
    }
    set {
      _value_in_specified_units = value;
      _value_as_string = this.to_string ();
    }
  }
  public double value_in_specified_units {
    get {
      return _value_in_specified_units;
    }
    set {
      _value_in_specified_units = value;
      _value_as_string = this.to_string ();
    }
  }

  public Length.with_values (float val, DomLength.Type units) {
    _value_in_specified_units = val;
    _unit_type = units;
    _value_as_string = this.to_string ();
  }

  public Length.with_units (DomLength.Type units) {
    _unit_type = units;
    _value_as_string = this.to_string ();
  }

  public string value_as_string {
    get {
      return _value_as_string;
    }
    set {
      _value_as_string = value;
      parse (_value_as_string);
    }
  }

  public void new_value_specified_units (DomLength.Type unit_type,
                                        double value_in_specified_units)
                                        throws GLib.Error {
    _unit_type = unit_type;
    this.value_in_specified_units = value_in_specified_units;
    this.value = this.value_in_specified_units;
    value_as_string = this.to_string ();
  }
  public void convert_to_specified_units (DomLength.Type unit_type) throws GLib.Error {
    GLib.warning ("No implemented");
  }
  public string to_string () {
    try {
      string units = GXml.Enumeration.get_string (typeof (DomLength.Type), _unit_type, true, true);
      if (_unit_type == DomLength.Type.UNKNOWN
          || _unit_type == DomLength.Type.NUMBER) units = "";
      if (_unit_type == DomLength.Type.PERCENTAGE) units = "%";
      string val = "%1.6g".printf (value_in_specified_units);
      return val+units;
    } catch { return ""; }
  }
  public void parse (string? v) {
    if (v == null) return;
    string rest = null;
    double vd = 0.0;
    double.try_parse (v, out vd, out rest);
    _value_in_specified_units = vd;
    _unit_type = DomLength.Type.UNKNOWN;
    if (rest != null) {
      try {
        if ("%" == rest)
          _unit_type = DomLength.Type.PERCENTAGE;
        else {
          var ev = GXml.Enumeration.parse (typeof (DomLength.Type), rest);
          _unit_type = (DomLength.Type) ev.value;
        }
      } catch { _unit_type = DomLength.Type.UNKNOWN; }
    }
    if (_value_as_string != v) {
      _value_as_string = this.to_string ();
    }
  }
}

public class GSvg.AnimatedLengthX : GSvg.AnimatedLength, DomAnimatedLengthX {
}

public class GSvg.AnimatedLengthY : GSvg.AnimatedLength, DomAnimatedLengthY {
}

public class GSvg.AnimatedLengthWidth : GSvg.AnimatedLength, DomAnimatedLengthWidth {
}
public class GSvg.AnimatedLengthHeight : GSvg.AnimatedLength, DomAnimatedLengthHeight {
}

public class GSvg.AnimatedLengthRX : GSvg.AnimatedLength, DomAnimatedLengthRX {
}

public class GSvg.AnimatedLengthRY : GSvg.AnimatedLength, DomAnimatedLengthRY {
}

public class GSvg.AnimatedLengthCX : GSvg.AnimatedLength, DomAnimatedLengthCX {
}

public class GSvg.AnimatedLengthCY : GSvg.AnimatedLength, DomAnimatedLengthCY {
}

public class GSvg.AnimatedLengthR : GSvg.AnimatedLength, DomAnimatedLengthR {
}

public class GSvg.LengthList : Gee.ArrayList<DomLength>,
                          DomLengthList
{
  private string separator = " ";
  public int number_of_items { get { return size; } }

  public new void  clear () throws GLib.Error { ((Gee.ArrayList<DomLength>) this).clear (); }
  public DomLength initialize (DomLength new_item) throws GLib.Error {
    add (new_item);
    return new_item;
  }
  public DomLength get_item (int index) throws GLib.Error {
    return get (index);
  }
  public DomLength insert_item_before (DomLength new_item, int index) throws GLib.Error {
    insert (index, new_item);
    return new_item;
  }
  public DomLength replace_item (DomLength new_item, int index) throws GLib.Error {
    remove_at (index);
    insert (index, new_item);
    return new_item;
  }
  public DomLength remove_item (int index) throws GLib.Error {
    return remove_at (index);
  }
  public DomLength append_item (DomLength new_item) throws GLib.Error {
    add (new_item);
    return new_item;
  }
  public string? value {
    set {
      if (" " in value) separator = " ";
      if ("," in value) separator = ",";
      string[] tks = value.split (separator);
      for (int i = 0; i < tks.length; i++) {
        var p = new Length ();
        p.parse (tks[i]);
        add (p as DomLength);
      }
    }
    owned get {
      if (size == 0) return null;
      string str = "";
      for (int i = 0; i < size; i++) {
        var p = get (i);
        str += p.value_as_string;
        if (i+1 < size) str += separator;
      }
      return str;
    }
  }
}
public class GSvg.AnimatedLengthList : GLib.Object,
                                      GXml.Property,
                                      DomAnimatedLengthList
{
  private DomLengthList _base_val;
  private DomLengthList _anim_val;
  // AnimatedLengthList
  public DomLengthList base_val {
    get {
      if (_base_val == null) _base_val = new LengthList ();
      return _base_val;
    }
  }
  public DomLengthList anim_val {
    get {
      if (_anim_val == null) _anim_val = new LengthList ();
      return _base_val;
    }
  }

  // GXml.Property
  public string? value {
    owned get {
      if (_base_val == null) return null;
      return base_val.value;
    }
    set {
      if (_base_val == null) _base_val = new LengthList ();
      base_val.value = value;
    }
  }
  public bool validate_value (string? val) {
    return "," in val || " " in val; // FIXME
  }
}
