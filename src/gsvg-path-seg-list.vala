/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* css-classes.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class GSvg.PathSegList : Gee.ArrayList<DomPathSeg>, DomPathSegList {

  public int number_of_items { get { return (int) this.size; } }

  public new void DomPathSegList.clear () throws GLib.Error {
    ((Gee.ArrayList<DomPathSeg>) this).clear ();
  }
  public DomPathSeg initialize (DomPathSeg new_item) throws GLib.Error {
    clear ();
    append_item (new_item);
    return new_item;
  }
  public DomPathSeg get_item (int index) throws GLib.Error {
    return this.get ((int) index);
  }
  public DomPathSeg insert_item_before (DomPathSeg new_item, int index) throws GLib.Error {
    this.insert ((int) index, new_item);
    return new_item;
  }
  public DomPathSeg replace_item (DomPathSeg new_item, int index) throws GLib.Error {
    var obj = this.remove_at ((int) index);
    this.insert ((int) index, new_item);
    return obj;
  }
  public DomPathSeg remove_item (int index) throws GLib.Error {
    return this.remove_at ((int) index);
  }
  public DomPathSeg append_item (DomPathSeg new_item) throws GLib.Error {
    add (new_item);
    return new_item;
  }

  public void parse (string str) throws GLib.Error {
    clear ();
    string s = str.replace ("\n", " ");
    s = s.replace (",", " ");
    message ("Str: %s", s);
    string unparsed = s;
    DomPathSeg current = null;
    double x, y;
    int i = 0;
    while (unparsed != null && unparsed != "" && i <= str.length) {
      string c = parse_command (unparsed, out unparsed);
      switch (c) {
        case "M":
          var m = new PathSegMovetoAbs ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          m.x = x;
          m.y = y;
          current = m;
          break;
        case "m":
          var mr = new PathSegMovetoRel ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          mr.x = x;
          mr.y = y;
          current = mr;
          break;
        case "z":
          var cp = new PathSegClosePath ();
          current = cp;
          break;
        case "Z":
          var cp = new PathSegClosePath ();
          current = cp;
          break;
        case "L":
          var l = new PathSegLinetoAbs ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          l.x = x;
          l.y = y;
          current = l;
          break;
        case "l":
          var lr = new PathSegLinetoRel ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          lr.x = x;
          lr.y = y;
          current = lr;
          break;
        case "H":
          var lh = new PathSegLinetoHorizontalAbs ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_x (unparsed, out x);
          lh.x = x;
          current = lh;
          break;
        case "h":
          var lhr = new PathSegLinetoHorizontalRel ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_x (unparsed, out x);
          lhr.x = x;
          current = lhr;
          break;
        case "V":
          var lv = new PathSegLinetoVerticalAbs ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_y (unparsed, out y);
          lv.y = y;
          current = lv;
          break;
        case "v":
          var lvr = new PathSegLinetoVerticalRel ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_y (unparsed, out y);
          lvr.y = y;
          current = lvr;
          break;
        case "C":
          var ca = new PathSegCurvetoCubicAbs ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          ca.x1 = x;
          ca.y1 = y;
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          ca.x2 = x;
          ca.y2 = y;
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          ca.x = x;
          ca.y = y;
          current = ca;
          break;
        case "c":
          var cr = new PathSegCurvetoCubicRel ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          cr.x1 = x;
          cr.y1 = y;
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          cr.x2 = x;
          cr.y2 = y;
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          cr.x = x;
          cr.y = y;
          current = cr;
          break;
        case "S":
          var cs = new PathSegCurvetoCubicSmoothAbs ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          cs.x2 = x;
          cs.y2 = y;
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          cs.x = x;
          cs.y = y;
          current = cs;
          break;
        case "s":
          var csr = new PathSegCurvetoCubicSmoothRel ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          csr.x2 = x;
          csr.y2 = y;
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          csr.x = x;
          csr.y = y;
          current = csr;
          break;
        case "Q":
          var q = new PathSegCurvetoQuadraticAbs ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          q.x1 = x;
          q.y1 = y;
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          q.x = x;
          q.y = y;
          current = q;
          break;
        case "q":
          var qr = new PathSegCurvetoQuadraticRel ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          qr.x1 = x;
          qr.y1 = y;
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          qr.x = x;
          qr.y = y;
          current = qr;
          break;
        case "T":
          var t = new PathSegCurvetoQuadraticSmoothAbs ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          t.x = x;
          t.y = y;
          current = t;
          break;
        case "t":
          var t = new PathSegCurvetoQuadraticSmoothRel ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          t.x = x;
          t.y = y;
          current = t;
          break;
        case "A":
          var ar = new PathSegArcAbs ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          ar.r1 = x;
          ar.r2 = y;
          x = y = 0.0;
          unparsed = DomPoint.parse_point_x (unparsed, out x);
          ar.angle = x;
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          ar.large_arc_flag = (bool) (int) x;
          ar.sweep_flag =  (bool) (int) y;
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          ar.x = x;
          ar.y = y;
          current = ar;
          break;
        case "a":
          var arr = new PathSegArcRel ();
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          arr.r1 = x;
          arr.r2 = y;
          x = y = 0.0;
          unparsed = DomPoint.parse_point_x (unparsed, out x);
          arr.angle = x;
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          arr.large_arc_flag = (bool) (int) x;
          arr.sweep_flag = (bool) (int) y;
          x = y = 0.0;
          unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
          arr.x = x;
          arr.y = y;
          current = arr;
          break;
      }
      append_item (current);
      i++;
    }
  }
}
