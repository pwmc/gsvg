/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-interfaces-struct.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using Gee;

namespace GSvg {
public interface DomGElement : GLib.Object,
                        DomElement,
                        DomTests,
                        DomLangSpace,
                        DomExternalResourcesRequired,
                        DomStylable,
                        DomTransformable,
                        DomContainerElement {
}

public interface DomGElementMap : GLib.Object {
  public abstract int length { get; }
  public abstract DomGElement get (string id);
  public abstract void append (DomGElement el);
}

public interface DomDescElement : GLib.Object,
                           DomElement,
                           DomLangSpace,
                           DomStylable {
}

public interface DomTitleElement : GLib.Object,
                            DomElement,
                            DomLangSpace,
                            DomStylable {
}

public interface DomElementInstance : GLib.Object, GXml.DomEventTarget {
  public abstract DomElement corresponding_element { get; }
  public abstract DomUseElement corresponding_use_element { get; }
  public abstract DomElementInstance parent_node { get; }
  public abstract DomElementInstanceList child_nodes { get; }
  public abstract DomElementInstance first_child { get; }
  public abstract DomElementInstance last_child { get; }
  public abstract DomElementInstance previous_sibling { get; }
  public abstract DomElementInstance next_sibling { get; }
}

public interface DomElementInstanceList : GLib.Object {

  public abstract uint length { get; }

  public abstract DomElementInstance item (uint index);
}

public interface DomGetSvgDocument {
  public abstract DomDocument get_svg_document();
}

} // GSvg
