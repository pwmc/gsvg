/* gsvg-dom-interfaces-fonts.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

namespace GSvg {

public interface DomFontElement : GLib.Object, DomSvgElement,
                           DomExternalResourcesRequired,
                           DomStylable {
}


public interface DomGlyphElement : GLib.Object, DomSvgElement,
                            DomStylable {
}

public interface DomMissingGlyphElement : GLib.Object, DomSvgElement,
                                   DomStylable {
}

public interface DomHKernElement : GLib.Object, DomSvgElement {
}

public interface DomVKernElement : GLib.Object, DomSvgElement {
}

public interface DomFontFaceElement : GLib.Object, DomSvgElement {
}

public interface DomFontFaceSrcElement : GLib.Object, DomSvgElement {
}

public interface DomFontFaceUriElement : GLib.Object, DomSvgElement {
}

public interface DomFontFaceFormatElement : GLib.Object, DomSvgElement {
}

public interface DomFontFaceNameElement : GLib.Object, DomSvgElement {
}

} // GSvg
