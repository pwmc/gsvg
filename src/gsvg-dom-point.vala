/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-pont.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

public interface GSvg.DomPoint : GLib.Object {

  public abstract double x { get; set; }
  public abstract double y { get; set; }

  public abstract DomPoint matrix_transform (DomMatrix matrix);
  public virtual void parse (string str) {
    string s = str.replace (",", " ");
    s = s.replace ("(", "");
    s = s.replace (")", "");
    double tx, ty;
    tx = ty = 0;
    parse_point_values (s, out tx, out ty);
    x = tx;
    y = ty;
  }
  public virtual string to_string () {
    return double_to_string (x)+","+double_to_string (y);
  }
  public static string double_to_string (double d) {
    string str = "%g".printf (d);
    return str.replace (",", ".");
  }

  public static string? parse_point_values (string str, out double x, out double y) {
    y = x = 0.0;
    string unparsed = null;
    int pos = skip_non_digits (str);
    if (pos == -1) {
      warning ("Invalid character found in parsing point's X value");
      return null;
    }
    if (pos == -2) {
      x = y = 0.0;
      return null;
    }
    double.try_parse (str.substring (pos, -1), out x, out unparsed);
    if (unparsed == null) {
      y = 0.0;
      return unparsed;
    }
    pos = skip_non_digits (unparsed);
    if (pos == -1) {
      warning ("Invalid character found in parsing point's Y value");
      return unparsed;
    }
    if (pos == -2) {
      x = y = 0.0;
      return null;
    }
    double.try_parse (unparsed.substring (pos, -1), out y, out unparsed);
    return unparsed;
  }

  public static string? parse_point_x (string str, out double x) {
    x = 0.0;
    string unparsed = null;
    int pos = skip_non_digits (str);
    if (pos == -1) {
      warning ("Invalid character found in parsing point's X value");
      return null;
    }
    if (pos == -2) {
      x = 0.0;
      return null;
    }
    double.try_parse (str.substring (pos, -1), out x, out unparsed);
    return unparsed;
  }

  public static string? parse_point_y (string str, out double y) {
    y = 0.0;
    string unparsed = null;
    int pos = skip_non_digits (str);
    if (pos == -1) {
      warning ("Invalid character found in parsing point's Y value");
      return null;
    }
    if (pos == -2) {
      y = 0.0;
      return null;
    }
    double.try_parse (str.substring (pos, -1), out y, out unparsed);
    return unparsed;
  }

  static int skip_non_digits (string str) {
    unichar c;
    for (int i = 0; i < str.length; i++) {
      if (!str.valid_char (i)) {
        warning ("Invalid character on skipping");
        return -1;
      }
      c = str.get_char (i);
      if (c.ispunct ()) {
        string cs = c.to_string ();
        if (cs == "-" || cs == "+") {
          return i;
        }
        if (cs != ".") {
          warning ("Unexpected character on skipping: %s", cs);
          return -1;
        }
      }
      if (c.isdigit ()) {
        return i;
      }
    }
    return -2;
  }
}

