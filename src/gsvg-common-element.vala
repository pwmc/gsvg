/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016,2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using Gee;

/**
 * Base class for SVG and basic types elements
 */
public class GSvg.CommonElement : Stylable,
                        DomTests,
                        DomLangSpace,
                        DomExternalResourcesRequired
{
  // Tests
  protected DomStringList _required_features;
  protected DomStringList _required_extensions;
  protected DomStringList _system_language;
  protected DomAnimatedBoolean _external_resources_required;
  protected DomElement _nearest_viewport_element;
  protected DomElement _farthest_viewport_element;
    // requiredFeatures
  public DomStringList required_features { get { return _required_features;} }
  // requiredExtensions
  public DomStringList required_extensions { get { return _required_extensions; } }
  // systemLanguage
  public DomStringList system_language { get { return _system_language; } }

  public bool has_extension (string extension) { return false; }
  // LangSpace
  [Description (nick="::xml:lang")]
  public string xmllang { get; set; }
  [Description (nick="::xml:space")]
  public string xmlspace { get; set; }
  // ExternalResourcesRequired
  // externalResourcesRequired
  public DomAnimatedBoolean external_resources_required {
    get { return _external_resources_required; }
  }
}
