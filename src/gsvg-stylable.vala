/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-gs-stylable.vala
 *
 * Copyright (C) 2016,2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using Gee;


public class GSvg.Stylable : GSvg.Element,
                        GSvg.DomStylable
{
  // Stylable
  public DomAnimatedString? class_name {
    get { return mclass_name; }
    set { mclass_name = value as AnimatedString; }
  }
  public DomCSSStyleDeclaration? style {
    get { return mstyle; }
    set { mstyle = value as CssStyleDeclaration; }
  }
  [Description (nick="::class")]
  public AnimatedString? mclass_name { get; set; }
  [Description (nick="::style")]
  public CssStyleDeclaration mstyle { get; set; }

  construct {
    set_instance_property ("style");
  }
}
