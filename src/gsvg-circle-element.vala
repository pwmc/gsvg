/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016, 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

public class GSvg.CircleElement : GSvg.Transformable,
                          GSvg.DomCircleElement, GXml.MappeableElement
{
  public DomAnimatedLengthCX cx { get; set; }
  [Description (nick="::cx")]
  public AnimatedLengthCX mcx {
    get { return cx as AnimatedLengthCX; }
    set { cx = value as DomAnimatedLengthCX; }
  }
  public DomAnimatedLengthCY cy { get; set; }
  [Description (nick="::cy")]
  public AnimatedLengthCY mcy {
    get { return cy as AnimatedLengthCY; }
    set { cy = value as DomAnimatedLengthCY; }
  }
  public DomAnimatedLengthR r { get; set; }
  [Description (nick="::r")]
  public AnimatedLengthR mr {
    get { return r as AnimatedLengthR; }
    set { r = value as DomAnimatedLengthR; }
  }
  construct {
    initialize ("circle");
  }
  // MappeableElement
  public string get_map_key () { return id; }

  public override bool pick (DomPoint point, double delta) {
    message ("Picking a Circle");
    if (r == null || cx == null || cy == null) return false;
    if (r.base_val == null || cx.base_val == null || cy.base_val == null) return false;
    double ccx, ccy, cr, cstroke;
    ccx = ccy = cr = cstroke = 0.0;
    var lstroke = stroke_width_to_lenght ();
    if (owner_document is GSvg.DomDocument) {
      var tsvg = ((GSvg.DomDocument) owner_document).root_element;
      if (tsvg != null) {
        ccx = tsvg.convert_length_x_pixels (cx.base_val);
        ccy = tsvg.convert_length_y_pixels (cy.base_val);
        cr = tsvg.convert_length_x_pixels (r.base_val);
        cstroke = tsvg.convert_length_x_pixels (lstroke);
      }
    } else {
      ccx = cx.base_val.value;
      ccy = cy.base_val.value;
      cr = r.base_val.value;
      cstroke = lstroke.value;
    }
    double vs = Math.pow ((point.x - ccx), 2) + Math.pow ((point.y - ccy), 2);
    double rl = Math.sqrt (vs);
    message ("(cx,cy,r, ep):(%g,%g,%g,%g)point(%g,%g)r=%g", ccx, ccy, cr, delta, point.x, point.y, rl);
    if (is_filled ()) {
      if (rl < cr + delta) {
        return true;
      }
    }
    double rmx = cr + cstroke + delta;
    double rmn = cr - cstroke - delta;
    message ("With stroke: (%g) rmx=%g rmn=%g", rl, rmx, rmn);
    if (rl < rmx && rl > rmn) {
      return true;
    }
    return false;
  }
}
