/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016,2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using Gee;

/**
 * An implementation of {@link ContainerElement}
 */
public class GSvg.ContainerElement : GSvg.CommonShapeElement, DomContainerElement {
  // ContainerElement
  private SvgElementMap _svgs_map;
  public DomSvgElementMap svgs { get { return svgs_map as DomSvgElementMap; } }
  public SvgElementMap svgs_map {
    get {
      if (_svgs_map == null)
        set_instance_property ("svgs-map");
      return _svgs_map;
    }
    set {
      if (_svgs_map != null) {
        try {
          clean_property_elements ("svgs-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _svgs_map = value;
    }
  }
  private GElementMap _groups_map;
  public DomGElementMap groups { get { return groups_map as DomGElementMap; } }
  public GElementMap groups_map {
    get {
      if (_groups_map == null)
        set_instance_property ("groups-map");
      return _groups_map;
    }
    set {
      if (_groups_map != null) {
        try {
          clean_property_elements ("groups-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _groups_map = value;
    }
  }
  private RectElementMap _rects_map;
  public DomRectElementMap rects { get { return rects_map as DomRectElementMap; } }
  public RectElementMap rects_map {
    get {
      if (_rects_map == null)
        set_instance_property ("rects-map");
      return _rects_map;
    }
    set {
      if (_rects_map != null) {
        try {
          clean_property_elements ("rects-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _rects_map = value;
    }
  }
  private CircleElementMap _circles_map;
  public DomCircleElementMap circles { get { return circles_map as DomCircleElementMap; } }
  public CircleElementMap circles_map {
    get {
      if (_circles_map == null)
        set_instance_property ("circles-map");
      return _circles_map;
    }
    set {
      if (_circles_map != null) {
        try {
          clean_property_elements ("circles-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _circles_map = value;
    }
  }
  private EllipseElementMap _ellipses_map;
  public DomEllipseElementMap ellipses { get { return ellipses_map as DomEllipseElementMap; } }
  public EllipseElementMap ellipses_map {
    get {
      if (_ellipses_map == null)
        set_instance_property ("ellipses-map");
      return _ellipses_map;
    }
    set {
      if (_ellipses_map != null) {
        try {
          clean_property_elements ("ellipses-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _ellipses_map = value;
    }
  }
  private LineElementMap _lines_map;
  public DomLineElementMap lines { get { return lines_map as DomLineElementMap; } }
  public LineElementMap lines_map {
    get {
      if (_lines_map == null)
        set_instance_property ("lines-map");
      return _lines_map;
    }
    set {
      if (_lines_map != null) {
        try {
          clean_property_elements ("lines-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _lines_map = value;
    }
  }
  private PolylineElementMap _polylines_map;
  public DomPolylineElementMap polylines { get { return polylines_map as DomPolylineElementMap; } }
  public PolylineElementMap polylines_map {
    get {
      if (_polylines_map == null)
        set_instance_property ("polylines-map");
      return _polylines_map;
    }
    set {
      if (_polylines_map != null) {
        try {
          clean_property_elements ("polylines-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _polylines_map = value;
    }
  }
  private PolygonElementMap _polygons_map;
  public DomPolygonElementMap polygons { get { return polygons_map as DomPolygonElementMap; } }
  public PolygonElementMap polygons_map {
    get {
      if (_polygons_map == null)
        set_instance_property ("polygons-map");
      return _polygons_map;
    }
    set {
      if (_polygons_map != null) {
        try {
          clean_property_elements ("polygons-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _polygons_map = value;
    }
  }
  private TextElementMap _texts_map;
  // ContainerElement
  public DomTextElementMap texts { get { return texts_map as DomTextElementMap; } }
  public TextElementMap texts_map {
    get {
      if (_texts_map == null)
        set_instance_property ("texts-map");
      return _texts_map;
    }
    set {
      if (_texts_map != null) {
        try {
          clean_property_elements ("texts-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _texts_map = value;
    }
  }

  private PathElementMap _paths_map;
  public DomPathElementMap paths { get { return paths_map as DomPathElementMap; } }
  public PathElementMap paths_map {
    get {
      if (_paths_map == null)
        set_instance_property ("paths-map");
      return _paths_map;
    }
    set {
      if (_paths_map != null) {
        try {
          clean_property_elements ("paths-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _paths_map = value;
    }
  }
  private ImageElementMap _images_map;
  public DomImageElementMap images { get { return images_map as DomImageElementMap; } }
  public ImageElementMap images_map {
    get {
      if (_images_map == null)
        set_instance_property ("images-map");
      return _images_map;
    }
    set {
      if (_images_map != null) {
        try {
          clean_property_elements ("images-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _images_map = value;
    }
  }

  private UseElementMap _uses_map;
  public DomUseElementMap uses { get { return _uses_map as DomUseElementMap; } }
  public UseElementMap uses_map {
    get {
      if (_uses_map == null)
        set_instance_property ("uses-map");
      return _uses_map;
    }
    set {
      if (_uses_map != null) {
        try {
          clean_property_elements ("uses-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _uses_map = value;
    }
  }
  private SwitchElementMap _switches_map;
  public DomSwitchElementMap switches { get { return _switches_map as DomSwitchElementMap; } }
  public SwitchElementMap switches_map {
    get {
      if (_switches_map == null)
        set_instance_property ("switches-map");
      return _switches_map;
    }
    set {
      if (_switches_map != null) {
        try {
          clean_property_elements ("switches-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _switches_map = value;
    }
  }
  private SymbolElementMap _symbols_map;
  public DomSymbolElementMap symbols { get { return _symbols_map as DomSymbolElementMap; } }
  public SymbolElementMap symbols_map {
    get {
      if (_symbols_map == null)
        set_instance_property ("symbols-map");
      return _symbols_map;
    }
    set {
      if (_symbols_map != null) {
        try {
          clean_property_elements ("symbols-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _symbols_map = value;
    }
  }
  private ClipPathElementMap _clip_paths_map;
  public DomClipPathElementMap clip_paths { get { return _clip_paths_map as DomClipPathElementMap; } }
  public ClipPathElementMap clip_paths_map {
    get {
      if (_clip_paths_map == null)
        set_instance_property ("clip-paths-map");
      return _clip_paths_map;
    }
    set {
      if (_clip_paths_map != null) {
        try {
          clean_property_elements ("clip-paths-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _clip_paths_map = value;
    }
  }
  private ScriptElementMap _scripts_map;
  public DomScriptElementMap scripts { get { return _scripts_map as DomScriptElementMap; } }
  public ScriptElementMap scripts_map {
    get {
      if (_scripts_map == null)
        set_instance_property ("scripts-map");
      return _scripts_map;
    }
    set {
      if (_scripts_map != null) {
        try {
          clean_property_elements ("scripts-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _scripts_map = value;
    }
  }
  private AnimateElementMap _animates_map;
  public DomAnimateElementMap animates { get { return _animates_map as DomAnimateElementMap; } }
  public AnimateElementMap animates_map {
    get {
      if (_animates_map == null)
        set_instance_property ("animates-map");
      return _animates_map;
    }
    set {
      if (_animates_map != null) {
        try {
          clean_property_elements ("animates-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _animates_map = value;
    }
  }
  private SetElementMap _sets_map;
  public DomSetElementMap sets { get { return _sets_map as DomSetElementMap; } }
  public SetElementMap sets_map {
    get {
      if (_sets_map == null)
        set_instance_property ("sets-map");
      return _sets_map;
    }
    set {
      if (_sets_map != null) {
        try {
          clean_property_elements ("sets-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _sets_map = value;
    }
  }
  private AnimateMotionElementMap _animate_motions_map;
  public DomAnimateMotionElementMap animate_motions { get { return _animate_motions_map as DomAnimateMotionElementMap; } }
  public AnimateMotionElementMap animate_motions_map {
    get {
      if (_animate_motions_map == null)
        set_instance_property ("animate-motions-map");
      return _animate_motions_map;
    }
    set {
      if (_animate_motions_map != null) {
        try {
          clean_property_elements ("animate-motions-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _animate_motions_map = value;
    }
  }
  private AnimateColorElementMap _animate_colors_map;
  public DomAnimateColorElementMap animate_colors { get { return _animate_colors_map as DomAnimateColorElementMap; } }
  public AnimateColorElementMap animate_colors_map {
    get {
      if (_animate_colors_map == null)
        set_instance_property ("animate-colors-map");
      return _animate_colors_map;
    }
    set {
      if (_animate_colors_map != null) {
        try {
          clean_property_elements ("animate-colors-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _animate_colors_map = value;
    }
  }
  private AnimateTransformElementMap _animate_transformations_map;
  public DomAnimateTransformElementMap animate_transformations { get { return _animate_transformations_map as DomAnimateTransformElementMap; } }
  public AnimateTransformElementMap animate_transformations_map {
    get {
      if (_animate_transformations_map == null)
        set_instance_property ("animate-transformations-map");
      return _animate_transformations_map;
    }
    set {
      if (_animate_transformations_map != null) {
        try {
          clean_property_elements ("animate-transformations-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _animate_transformations_map = value;
    }
  }
  private LinearGradientElementMap _linear_gradients_map;
  public DomLinearGradientElementMap linear_gradients { get { return _linear_gradients_map as DomLinearGradientElementMap; } }
  public LinearGradientElementMap linear_gradients_map {
    get {
      if (_linear_gradients_map == null)
        set_instance_property ("linear-gradients-map");
      return _linear_gradients_map;
    }
    set {
      if (_linear_gradients_map != null) {
        try {
          clean_property_elements ("linear-gradients-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _linear_gradients_map = value;
    }
  }
  private RadialGradientElementMap _radial_gradients_map;
  public DomRadialGradientElementMap radial_gradients { get { return _radial_gradients_map as DomRadialGradientElementMap; } }
  public RadialGradientElementMap radial_gradients_map {
    get {
      if (_radial_gradients_map == null)
        set_instance_property ("radial-gradients-map");
      return _radial_gradients_map;
    }
    set {
      if (_radial_gradients_map != null) {
        try {
          clean_property_elements ("radial-gradients-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _radial_gradients_map = value;
    }
  }
  private DefsElementMap _defs_map;
  public DomDefsElementMap defs { get { return _defs_map as DomDefsElementMap; } }
  public DefsElementMap defs_map {
    get {
      if (_defs_map == null)
        set_instance_property ("defs-map");
      return _defs_map;
    }
    set {
      if (_defs_map != null) {
        try {
          clean_property_elements ("defs-map");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _defs_map = value;
    }
  }
}
