/* gsvg-path-element.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


public interface GSvg.DomPathElement : GLib.Object, DomElement,
                           DomTests,
                           DomLangSpace,
                           DomExternalResourcesRequired,
                           DomStylable,
                           DomTransformable,
                           DomAnimatedPathData {

  public abstract string d { get; set; }
  public abstract DomAnimatedNumber path_length { get; construct set; }

  public abstract double get_total_length ();
  public abstract DomPoint get_point_at_length (double distance);
  public abstract uint get_path_seg_at_length (double distance);
  public abstract DomPathSegClosePath create_svg_path_seg_close_path ();
  public abstract DomPathSegMovetoAbs create_svg_path_seg_moveto_abs (double x, double y);
  public abstract DomPathSegMovetoRel create_svg_path_seg_moveto_rel (double x, double y);
  public abstract DomPathSegLinetoAbs create_svg_path_seg_lineto_abs (double x, double y);
  public abstract DomPathSegLinetoRel create_svg_path_seg_lineto_rel (double x, double y);
  public abstract DomPathSegCurvetoCubicAbs create_svg_path_seg_curveto_cubic_abs (double x, double y, double x1, double y1, double x2, double y2);
  public abstract DomPathSegCurvetoCubicRel create_svg_path_seg_curveto_cubic_rel (double x, double y, double x1, double y1, double x2, double y2);
  public abstract DomPathSegCurvetoQuadraticAbs create_svg_path_seg_curveto_quadratic_abs (double x, double y, double x1, double y1);
  public abstract DomPathSegCurvetoQuadraticRel create_svg_path_seg_curveto_quadratic_rel (double x, double y, double x1, double y1);
  public abstract DomPathSegArcAbs create_svg_path_seg_arc_abs (double x, double y, double r1, double r2, double angle, bool largeArcFlag, bool sweepFlag);
  public abstract DomPathSegArcRel create_svg_path_seg_arc_rel (double x, double y, double r1, double r2, double angle, bool largeArcFlag, bool sweepFlag);
  public abstract DomPathSegLinetoHorizontalAbs create_svg_path_seg_lineto_horizontal_abs (double x);
  public abstract DomPathSegLinetoHorizontalRel create_svg_path_seg_lineto_horizontal_rel (double x);
  public abstract DomPathSegLinetoVerticalAbs create_svg_path_seg_lineto_vertical_abs (double y);
  public abstract DomPathSegLinetoVerticalRel create_svg_path_seg_lineto_vertical_rel (double y);
  public abstract DomPathSegCurvetoCubicSmoothAbs create_svg_path_seg_curveto_cubic_smooth_abs (double x, double y, double x2, double y2);
  public abstract DomPathSegCurvetoCubicSmoothRel create_svg_path_seg_curveto_cubic_smooth_rel (double x, double y, double x2, double y2);
  public abstract DomPathSegCurvetoQuadraticSmoothAbs create_svg_path_seg_curveto_quadratic_smooth_abs (double x, double y);
  public abstract DomPathSegCurvetoQuadraticSmoothRel create_svg_path_seg_curveto_quadratic_smooth_rel (double x, double y);
}


