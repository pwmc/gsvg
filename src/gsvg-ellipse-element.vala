/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016, 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

public class GSvg.EllipseElement : GSvg.Transformable,
                          GSvg.DomEllipseElement, GXml.MappeableElement
{
  public DomAnimatedLengthCX cx  { get; set; }
  [Description (nick="::cx")]
  public AnimatedLengthCX mcx {
    get { return cx as AnimatedLengthCX; }
    set { cx = value as DomAnimatedLengthCX; }
  }
  public DomAnimatedLengthCY cy { get; set; }
  [Description (nick="::cy")]
  public AnimatedLengthCY mcy {
    get { return cy as AnimatedLengthCY; }
    set { cy = value as DomAnimatedLengthCY; }
  }
  public DomAnimatedLengthRX rx { get; set; }
  [Description (nick="::rx")]
  public AnimatedLengthRX mrx {
    get { return rx as AnimatedLengthRX; }
    set { rx = value as DomAnimatedLengthRX; }
  }
  public DomAnimatedLengthRY ry { get; set; }
  [Description (nick="::ry")]
  public AnimatedLengthRY mry {
    get { return ry as AnimatedLengthRY; }
    set { ry = value as DomAnimatedLengthRY; }
  }
  construct {
    try { initialize ("ellipse"); }
    catch (GLib.Error e) { warning ("Error: "+e.message); }
  }
  // MappeableElement
  public string get_map_key () { return id; }

  public override bool pick (DomPoint point, double delta) {
    if (cx == null || cy == null || rx == null || ry == null) {
      return false;
    }
    double ccx, ccy, crx, cry, cstroke;
    ccx = ccy = crx = cry = cstroke = 0.0;
    var lstroke = stroke_width_to_lenght ();
    if (owner_document is GSvg.DomDocument) {
      var tsvg = ((GSvg.DomDocument) owner_document).root_element;
      if (tsvg != null) {
        ccx = tsvg.convert_length_x_pixels (cx.base_val);
        ccy = tsvg.convert_length_y_pixels (cy.base_val);
        crx = tsvg.convert_length_x_pixels (rx.base_val);
        cry = tsvg.convert_length_y_pixels (ry.base_val);
        cstroke = tsvg.convert_length_x_pixels (lstroke);
      }
    } else {
      ccx = cx.base_val.value;
      ccy = cy.base_val.value;
      crx = rx.base_val.value;
      cry = ry.base_val.value;
      cstroke = lstroke.value;
    }
    double p = 1 - Math.pow (point.x - ccx, 2) / Math.pow (crx, 2) - Math.pow (point.y - ccy, 2) / Math.pow (cry, 2);
    if (is_filled ()) {
      if (p >= 0) {
        return true;
      }
    }
    if (Math.fabs (p) < (((delta + cstroke)*1.5) / ccx)) {
      return true;
    }
    return false;
  }
}

