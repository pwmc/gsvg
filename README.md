# Introduction

GSVG is an effort to provide a GLib GObject implementation of W3C standard API
to create and edit SVG files.

GSVG relays on GXml's to provide W3C DOM4 API.

# Documentation

You can access this project's API [Documentation](https://gsvg.gitlab.io/gsvg/)

# Dependencies

GSVG depends on:

* GXml > 0.20.3 [source](https://gitlab.gnome.org/GNOME/gxml)
